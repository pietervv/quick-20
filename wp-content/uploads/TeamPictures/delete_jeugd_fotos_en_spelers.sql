-- Verwijder ook de daadwerkelijke plaatjes uit wp-content/uploads/TeamPictures

DELETE FROM webservice__user
WHERE team IN (
	SELECT knvb_id FROM webservice__team
    WHERE category IN ('Junioren', 'Pupillen')
);

DELETE FROM webservice__team_data
WHERE team_id IN (
	SELECT knvb_id FROM webservice__team
    WHERE category IN ('Junioren', 'Pupillen')
);