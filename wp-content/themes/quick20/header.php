<!DOCTYPE html>
<html lang="en">

<!--
                BBBBB          
               BBBBBBBBBB              
               BBBBBBBBBBBBB           
               BBBBBBBBBBBBBBBB
               BBBBBBBBBBBBBBBBBBB      
   BBBB        BBB   BBBBBBBBBBBBBBB   
BBBBBBB                BBBBBBBBBBBBBB
BBBBBBB                   BBBBBBBBBBBBB
BBBBBBB                      BBBBBBBBBB
BBBBBBB          BBBBB          BBBBBBB    ###############################
BBBBBBB        BBBBBBBBB        BBBBBBB    #          BRANDCUBE          #
BBBBBBB        BBBBBBBBB        BBBBBBB    #       Digital Creation      #
BBBBBBB        BBBBBBBBB        BBBBBBB    #     Tel. +31 0541-820009    #
BBBBBBB        BBBBBBBBB        BBBBBBB    #      info@brandcube.nl      #
BBBBBBB        BBBBBBBBB        BBBBBBB    #       www.brandcube.nl      #
BBBBBBB          BBBBB          BBBBBBB    ###############################
BBBBBBBBBB                   BBBBBBBBBB
BBBBBBBBBBBBB             BBBBBBBBBBBBB
BBBBBBBBBBBBBB         BBBBBBBBBBBBBB
   BBBBBBBBBBBBBBB   BBBBBBBBBBBBBBB   
      BBBBBBBBBBBBBBBBBBBBBBBBBBB
         BBBBBBBBBBBBBBBBBBBBB         
           BBBBBBBBBBBBBBBBB           
              BBBBBBBBBBB              
                 BBBBB
-->

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link title="Favicon" href="<?php bloginfo('template_directory'); ?>/images/logo-quick20-jubileum.png" rel="shortcut icon" />
    <title><?php echo get_the_title(); ?> | Quick'20 Oldenzaal</title>
	<link href='https://fonts.googleapis.com/css?family=Rokkitt:300,400,500,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Rokkitt' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Rokkitt:300&display=swap" rel="stylesheet"> 
	
	<link href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css" rel="stylesheet">	
	<link href="<?php bloginfo('template_directory'); ?>/css/unslider.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/jquery.simplyscroll.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/main_20190903.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/jubileum_20200110.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-5025235-1', 'auto');
		  ga('send', 'pageview');

	  </script>



  </head>
  <body>
	  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.5&appId=331527873547292";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <header>
	    <div class="container">
		    <div class="col-lg-offset-1 col-md-2 col-xs-2 logo">
			    <a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-quick20-jubileum.png"></a>
		    </div>
		    <div class="col-md-10 col-lg-8 col-xs-10 navigation">
			    <nav class="navbar">
				    
				    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				     </button>
				    
			    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						 <?php
							/** Loading WordPress Custom Menu  **/
								wp_nav_menu( array(
								'theme_locator'   => 'primary-menu',
								'menu'            => 'primary-menu',
								'container_class' => 'navbar-collapse',
								'menu_class'      => 'nav navbar-nav navbar-left',
								'fallback_cb'     => '',
								'walker' => new wp_bootstrap_navwalker()
							) ); ?>
					</div>
				</nav>
		    </div>
		    <?php $currUser = wp_get_current_user();
		    if ($currUser != null && $currUser->ID != 0) { ?>
			    <div class="LoggedIn">
			    	Ingelogd als: <?php echo $currUser->user_login; ?>
			    </div>
			<?php } ?>
	    </div>
    </header>