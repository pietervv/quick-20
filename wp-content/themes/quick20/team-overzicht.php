<?php
/*
Template Name: Team-overzicht - Senioren
*/
?>

<?php get_header(); global $wpdb; ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Seniorenteams</h1>
		    </div>
	    </div>
    </div>
    
	
	<div class="TeamOverview">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Vrijdag</div>
					<?php
					$friday = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'ZO' AND short_name LIKE '%+%' AND short_name NOT LIKE '%VR%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($friday as $team) {
						?><a href="../teams/senioren?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo 'Vrijdag ' . $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Zaterdag</div>
					<?php
					$saturday = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'ZA' AND short_name NOT LIKE '%+%' AND short_name NOT LIKE '%VR%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($saturday as $team) {
						?><a href="../teams/senioren?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo 'Zaterdag ' . $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Zondag</div>
					<?php
					$sunday = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'ZO' AND short_name NOT LIKE '%+%' AND short_name NOT LIKE '%VR%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($sunday as $team) {
						?><a href="../teams/senioren?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo 'Zondag ' . $team['short_name']; ?></button></a>
						<?php
					}
   					?>

				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Vrouwen</div>
					<?php
					$women = $wpdb->get_results("SELECT * FROM webservice__team WHERE `category` = 'Senioren' AND name LIKE '%VR%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($women as $team) {
						?><a href="../teams/senioren?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php if (strpos($team['name'], '(ZA)') !== false) echo 'Zaterdag '; else echo 'Zondag '; echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>

				</div>
				<div class="col-md-2 col-md-20 col-sm-100 col-xs-12 TeamList">
					<div class="MoreInfo">Meer Info</div>
					<a href="<?php the_field('link_download_trainingschema'); ?>"><div class="btn-side btn-green">Download Trainingsschema</div></a>
					<a href="<?php the_field('link_lid_worden'); ?>"><div class="btn-side btn-red">Ik wil lid worden</div></a>
					<a href="<?php the_field('link_vrijwilliger_worden'); ?>"><div class="btn-side btn-white">Ik wil vrijwilliger worden</div></a>
					<a href="<?php the_field('link_sponsor_worden'); ?>"><div class="btn-side btn-black">Ik wil sponsor worden</div></a>
				</div>
			</div>
		</div>
	</div>	


	<?php include 'footer.php';?>