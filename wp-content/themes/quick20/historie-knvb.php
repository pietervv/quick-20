<?php
/*
Template Name Posts: Historie-knvb
Template Name: Historie-knvb

*/
?>
	<?php get_header(); ?>
	<div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Historie</h1>
		    </div>
	    </div>
	</div>
	
	<div class="Templates">
		<div class="row TeamRowPlayer">
			<div class="col-md-7 col-xs-6 PlayerName"></div>
			<div class="col-md-5 col-xs-6 PlayerPosition"></div>
		</div>
		<div class="StandingsDiv row">
			<div class="StandingsTitle"></div>
			<table class="KnvbTable StandingsTable">
				<tr class="TitleRow">
					<th>#</th>
					<th>Team</th>
					<th>G</th>
					<th>W</th>
					<th>GL</th>
					<th>V</th>
					<th>Pnt</th>
					<th>DS</th>
					<th>PM</th>
				</tr>
			</table>
		</div>
		<div class="row ResultsTable">
			<h3 class="RowTitle RoundTitle">Uitslagen </h3>
			<table class="KnvbTable ResultsTable">
				<tr class="TitleRow">
					<th>thuisploeg</th>
					<th>uitploeg</th>
					<th>uitslag</th>
				</tr>
			</table>
		</div>
	</div>
    
	<div class="TeamSingle History">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<div class="col-md-3 col-xs-12 Sidebar">
					<div class="row MenuFilters">
						<div class="title">Kies een team</div>
						<select name="team" class="team UnSelected">
							<option disabled selected value>-- Team --</option>
						</select>
						<div class="title">Kies een seizoen</div>
						<select name="season" class="season UnSelected">
							<option disabled selected value class="initial">-- Seizoen --</option>
						</select>
					</div>
					<ul class="ContentSwitch">
						<li class="SwitchItem active" value="Players">Spelers</li>
						<li class="SwitchItem" value="Results">Uitslagen</li>
						<li class="SwitchItem" value="Standings">Stand</li>
						<li class="SwitchItem" value="Cup">Beker</li>
						<li class="SwitchItem" value="Playoffs">Nacompetitie</li>
					</ul>
				</div>
				<div class="col-md-9 col-xs-12 ContentBlock">
					<div class="Content Players">
						<span class="NotSelected"><h3>Selecteer eerst een team en seizoen.</h3></span>
						<div class="Selected">
							<div class="col-xs-12">
								<div class="TeamTitle row"></div>
								<div class="TeamPlayers row"></div>
							</div>
						</div>
					</div>
					<div class="Content Results">
						<span class="NotSelected"><h3>Selecteer eerst een team en seizoen.</h3></span>
						<div class="Selected">
							<div class="col-xs-12">
								<div class="TeamTitle row"></div>
								<div class="ResultsDiv row"></div>
							</div>
						</div>
					</div>
					<div class="Content Standings">
						<span class="NotSelected"><h3>Selecteer eerst een team en seizoen.</h3></span>
						<div class="Selected">
							<div class="TeamTitle row"></div>
							<div class="col-xs-12 StandingsContainer"></div>
						</div>
					</div>
					<div class="Content Cup">
						<span class="NotSelected"><h3>Selecteer eerst een team en seizoen.</h3></span>
						<div class="Selected">
							<div class="row TeamTitle"></div>
							<div class="col-xs-12 CupDiv"></div>
						</div>
					</div>
					<div class="Content Playoffs">
						<span class="NotSelected"><h3>Selecteer eerst een team en seizoen.</h3></span>
						<div class="Selected">
							<div class="row TeamTitle"></div>
							<div class="col-xs-12 PlayOffsDiv"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<?php include 'footer.php';?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.historie.js"></script>