<?php
/*
Template Name: Agenda
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
		    </div>
	    </div>
    </div>
    
	
	<div class="AgendaPage">
		<div class="container">
			<!--
			<?php
			if( have_rows('evenementen') ):
			while ( have_rows('evenementen') ) : the_row(); ?>

			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<div class="col-md-12 col-left"><h2 class="h2Agenda"><?php the_sub_field('naam_maand'); ?></h2></div>
			</div>
			<div class="col-lg-offset-1 col-lg-8 col-xs-10 AgendaMonth">
				<?php
				if( have_rows('event') ):
				while ( have_rows('event') ) : the_row(); ?>
				<div class="row RowAgenda">
					<div class="col-xs-4 DateColumn"><?php the_sub_field('datum'); ?></div>
					<div class="col-xs-2 TimeColumn"><?php the_sub_field('tijd'); ?></div>
					<div class="col-xs-4 EvenColumn"><strong><?php the_sub_field('naam_event'); ?></strong></div>
				</div>
				<?php
				endwhile;
				else :
				endif;
   				?>
			</div>
			<?php
			endwhile;
			else :
			endif;
   			?>
   			
   			-->
   			
   			
<?php
$args = array (
    'post_type' => 'kalender',
    'posts_per_page' => '-1',
    'meta_key' => 'datum',
    'orderby' => 'meta_value',
    'order' => 'ASC'
    
);
$the_query = new WP_Query( $args );

$months = ['', 'januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];
$daysOfTheWeek = ['', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag', 'zondag'];
$this_month = 0;
$previous_month = 0;

$allPosts = get_posts($args);
?>

<?php if( $the_query->have_posts() ): ?>
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

	<?php
		$now = time();
		$tomorrow = $now+86400;
		$post_date = get_field('datum');
		
		$this_month = date('n', strtotime($post_date));
		
		$post_timestamp = strtotime($post_date);
		$dateString  = $daysOfTheWeek[date('N', $post_timestamp)];
		$dateString .= ' '.date('j', $post_timestamp);
		$dateString .= ' '.$months[date('n', $post_timestamp)];
		?>
		
		
		<?php if($now < $post_timestamp) : ?>
			
			<?php if($this_month != $previous_month) : ?>
		
			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<div class="col-md-12 col-left"><h2 class="h2Agenda"><?= $months[$this_month].' '.date('Y', strtotime($post_date)); ?></h2></div>
			</div>
			<?php endif; ?>
		
				
            <div class="col-lg-offset-1 col-lg-8 col-xs-12 col-md-10 agenda-row">
    			<div class="row RowAgenda">
					<div class="col-xs-4 DateColumn"><?= $dateString; ?></div>
					<div class="col-xs-2 TimeColumn"><?php the_field('tijd'); ?> uur</div>
					<div class="col-xs-6 EvenColumn"><strong><?php echo get_the_title(); ?></strong></div>
				</div>
    		</div>
    		<?php $previous_month = $this_month; ?>
    	<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); 
?>
   			
		</div>
	</div>
<div class="AtmospherePics">
	<div class="col-md-4 AtmospherePicture" style="background-image: url(<?php the_field('sfeer_foto_1'); ?>);"></div>
	<div class="col-md-4 AtmospherePicture" style="background-image: url(<?php the_field('sfeer_foto_2'); ?>);"></div>
	<div class="col-md-4 AtmospherePicture" style="background-image: url(<?php the_field('sfeer_foto_3'); ?>);"></div>
</div>

		<?php include 'footer.php';?>