<?php
/*
 Template Name: Jublieum-loterij
 
 */
?>
<?php get_header(); ?>
<div class="JubileumPage Loterij">
	<div class="MainTitle">
		<div class="container ContainerMainTitle">
			<div class="col-xs-12">˙
				<h1 class="Title">Loterij</h1>
			</div>
		</div>
	</div>
	<div class="ImageHeader">
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_links'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_midden'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_rechts'); ?>);">
		</div>
	</div>
	<div class="container">
		<div class="col-lg-offset-2 col-lg-8 col-md-8">
			<div class="Introduction">
				<div class="col-xs-12">
					<h2 class="black">Jubileumloterĳ</h2>
				</div>
				<?php the_field('inleiding'); ?>
			</div>
			<div class="Programma">
			
			<!-- aanmelden -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Speel mee!</h2>
					</div>
					<div class="col-xs-12">
        				<div class="PreLink">
    						<form method="post" class="wpcf7-form">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="11788">
                                    <input type="hidden" name="_wpcf7_version" value="4.3.1">
                                    <input type="hidden" name="_wpcf7_locale" value="nl_NL">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11788-o1">
                                    <input type="hidden" name="_wpnonce" value="3b3c6ac5fa">
                                    <input type="hidden" name="formId" value="loterij">
                                </div>
                                <p>
                                    Door verzending van dit formulier geef ik toestemming aan KVV Quick '20 te Oldenzaal
                                    (incassant ID: NL14ZZZ400730440000) om een <b>eenmalige</b> incasso-opdracht te sturen
                                    naar mijn bank om een bedrag van <b>EUR 30,00 per lot</b> van mijn rekening af te schrijven.
                                </p>
                                <p>
                                	Na betaling ontvangt u per e-mail of per post uw lot(en) met uw unieke lotnummer(s).
                                </p>
                                <p>Voorletter(s)<br>
                                	<span class="wpcf7-form-control-wrap Voorletters">
                                		<input required type="text" name="Voorletters" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span>
                                </p>
                                <p>Achternaam<br>
                                	<span class="wpcf7-form-control-wrap Achternaam">
                                		<input required type="text" name="Achternaam" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span>
                                </p>
                                <p>Plaats<br> 
                                	<span class="wpcf7-form-control-wrap Plaats">
                                		<input type="text" name="Plaats" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>Tel. / mobiel<br> 
                                	<span class="wpcf7-form-control-wrap Telefoon">
                                		<input required type="text" name="Telefoon" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>E-mail<br> 
                                	<span class="wpcf7-form-control-wrap Email">
                                		<input required type="text" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>Aantal loten: 
                                	<span class="wpcf7-form-control-wrap Aantal_loten">
                                		<input type="number" name="Aantal_loten" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" min="0" max="99"  placeholder="0">
                                	</span>
                                </p>
                                <div class="vorm">
                                    <p>Vorm loten (per post is niet meer mogelijk):<br>
                                    	<span class="wpcf7-form-control-wrap Vorm">
                                    		<span class="wpcf7-form-control wpcf7-radio">
                                    			<span class="wpcf7-list-item first">
                                    				<input type="radio" name="Vorm" value="Per e-mail" checked>&nbsp;
                                    				<span class="wpcf7-list-item-label">Per e-mail</span>
                                    			</span>
                                    		</span>
                                    	</span>
                                    </p>
                                </div>
                               <p>
                               		<span class="wpcf7-form-control-wrap Akkoord">
                               			<input required type="checkbox" name="AkkoordRegels" class="wpcf7-form-control wpcf7-acceptance akkoord" >
                               		</span>
                               		Ik verklaar 18 jaar of ouder te zijn en akkoord te gaan met de loterijregels.
                               		De regels zijn te bekijken op deze pagina.
                               	</p>
                               	<p>
                               		<span class="wpcf7-form-control-wrap Akkoord">
                               			<input required type="checkbox" name="AkkoordMachtiging" class="wpcf7-form-control wpcf7-acceptance akkoord" >
                               		</span>
                               		Ik ga akkoord met de eenmalige machtiging.
                               	</p>
                                <p>Datum<br> 
                                	<span class="wpcf7-form-control-wrap Datum">
                                		<input required type="text" name="Datum" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>IBAN<br> 
                                	<span class="wpcf7-form-control-wrap IBAN">
                                		<input required type="text" name="IBAN" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>Naam verkoper:<br> 
                                	<span class="wpcf7-form-control-wrap IBAN">
                                		<input type="text" name="NaamVerkoper" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>Team verkoper:<br> 
                                	<span class="wpcf7-form-control-wrap IBAN">
                                		<input type="text" name="TeamVerkoper" value="" size="40" class="wpcf7-form-control wpcf7-text" >
                                	</span> 
                                </p>
                                <p>
                                	<button id="submitModalForm">Verzenden</button>
                                </p>
                                <p>
                                	<input type="submit" value="VerzendenHidden" id="submitModalFormHidden">
                                </p>
                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                        	</form>
            			</div>
            			<div class="Sending">
        					Uw inschrijving wordt verzonden
        					<img src="<?php bloginfo('template_directory'); ?>/images/loading.gif" />
        				</div>
        				<div class="Success">Uw inschrijving is verzonden!</div>
        			</div>
				</div>
			
			<!-- prijzen -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Prijzen</h2>
					</div>
					<div class="col-xs-12">
						<?php
                            $file = get_field('prijzenoverzicht');
                            if($file): ?>
                                <a href="<?php the_field('prijzenoverzicht'); ?>">
                                	Klik hier om alle prijzen te bekijken.
                                </a>
                         <?php endif; ?>
        			</div>
				</div>
				
				<!-- trekking -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Trekking</h2>
					</div>
					<div class="col-xs-12">
						<p>
						De trekkingen van de Jubileumloterĳ van Quick'20 vinden op de
                        volgende dagen plaats:
                            <ul>
                            	<li>20 januari 2020</li>
                            	<li>20 februari 2020</li>
                            	<li>20 maart 2020</li>
                            	<li>20 april 2020</li>
                            	<li>20 mei 2020</li>
                            	<li>13 juni 2020</li>
                            </ul>
                        </p>
                        <p>
                        De trekkingen worden verricht door notaris Riteco te Oldenzaal.
                        De maandelijkse trekkingen vinden plaats te Oldenzaal en de uitslagen
                        worden na de trekkingen gepubliceerd op deze website.
                        Kansspelbelasting voor rekening vergunninghouder. Prijswinnaars
                        worden persoonlijk per mail en/of telefoon op de hoogte gesteld.
                        Wijzigingen voorbehouden.
                        </p><p>
                        De vergunning voor deze loterij met 5.000 loten voor een bedrag van
                        € 30,- per lot is verstrekt aan KVV Quick'20 te Oldenzaal onder
                        kenmerk 13582 op 10/10/2019 door de Kansspelautoriteit. Voor vragen
                        kunt u per e-mail contact opnemen: loterij100jaar@quick20.nl
                        </p>
        			</div>
				</div>
				
				<!-- spelregels -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Loterijregels</h2>
					</div>
					<div class="col-xs-12">
						<?php
                            $file = get_field('spelregels');
                            if($file): ?>
                                <a href="<?php the_field('spelregels'); ?>">
                                	Klik hier om de loterijregels te bekijken.
                                </a>
                         <?php endif; ?>
        			</div>
				</div>
				
				<!-- winnaars -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Winnaars</h2>
					</div>
					<div class="col-xs-12">
						Na de eerste trekking worden de winnende loten hier bekend gemaakt.
						Houd deze pagina dus goed in de gaten!
        			</div>
				</div>
        	</div>
        	<div class="col-xs-12">
				<div class="row JubileumSlider">
        			<ul id="jubileumScroller">
        				<?php
        				if( have_rows('hoofdsponsors', 'option') ):
        				while ( have_rows('hoofdsponsors', 'option') ) : the_row(); ?>
        					<li><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('logo', 'option'); ?>"></a></li>
        				<?php
        				endwhile;
        				else :
        				endif;
        				?>
        			</ul>  
        		</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="AanmeldingsModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="AanmeldingsModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4>Inschrijfformulier</h4>
			</div>
			<div class="modal-body">
				<div id="Data"></div>
				<div class="Sending">
					Uw inschrijving wordt verzonden
					<img src="<?php bloginfo('template_directory'); ?>/images/loading.gif" />
				</div>
				<div class="Success">Uw inschrijving is verzonden!</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php';?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileumpage.20200105.js"></script>