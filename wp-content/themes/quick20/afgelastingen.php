<?php
/*
Template Name: Afgelastingen
*/
?>
<?php get_header(); ?>
<div class="MainTitle">
    <div class="container ContainerMainTitle">
	    <div class="col-xs-12">
	    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
	    </div>
    </div>
</div>
<div class="ProgrammaPage">
	<div class="container">
		<div class="col-lg-offset-1 col-lg-10 col-xs-12">
			<div class="row title">
				<div class="CanceledContainer AlwaysShow col-xs-12">
					<h2>Afgelastingen</h2>
					<table class="CanceledTable">
						<tr class="HeaderRow">
							<th class="one">Datum</th>
							<th class="two">Tijd</th>
							<th class="three">Thuisploeg</th>
							<th class="four">Uitploeg</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.canceled.js"></script>