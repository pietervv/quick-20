<?php
/*
Template Name: Trainingsschema
*/
?>
<?php get_header(); ?>

<div class="MainTitle">
    <div class="container ContainerMainTitle">
	    <div class="col-xs-12">
	    	<h1 class="Title">Trainingsschema</h1>
	    </div>
    </div>
</div>
    
<div class="TrainingClub">
	<div class="container">
		<div class="DayContainer">
			<div class="Template col-xs-12">
				<div class="col-xs-12">
					<h2 class="black Header"><span class="DayHeader"></span></h2>
				</div>
				<table>
					<tr class="HeaderRow">
						<th class="one">Tijdstip</th>
						<th class="two">Team</th>
						<th class="three">Veld</th>
						<th class="four">Klk</th>
						<th class="five HideSmall">Opmerking</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php'; ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.trainingen.20181227.js"></script>