<?php
/*
Template Name: Homepagina
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : ?>
    <div class="slider my-slider">
	    <ul>
		<?php $count = 0; ?>
		<?php
		$displayposts = new WP_Query();
		$displayposts->query('post');
		while ($displayposts->have_posts()) : $displayposts->the_post(); ?>

					
			<?php

				$values = get_field('in_slider');
				if(is_array($values) && in_array("ja", $values)) {

			?>

			<?php $count++; ?>
			<?php if ($count<6) : ?>
			<li>
			    <?php if(get_field('hoofdafbeelding')): ?>
				    <div class="slider-container <?php the_field('uitlijnen_afbeelding'); ?>" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);">
				<?php else: ?>	    
					    
					<?php if (in_category('algemeen') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('interactief') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('wedstrijdprogrammas') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('vrijwilligers') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('1e-selectie') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('senioren') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('sponsoren') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);">
					<?php endif; ?>
					<?php if (in_category('jeugd') ) : ?>
						<div class="slider-container <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);">
					<?php endif; ?>
					    
				<?php endif; ?> 	    

					    <div class="slider-overlay"></div>
					    <div class="container">
							<div class="col-lg-offset-1 col-md-8 slider-caption">
								<h1><?php echo get_the_title(); ?></h1>
								<div class="slideCaptionLine"></div>
								<p><?php echo ''.get_the_twitter_excerpt(); ?>...</p>
								<a href="<?php the_permalink(); ?>"><button class="btn btn-readmore">Lees meer</button></a>
							</div>
					    </div>
			    	</div>
		    </li>
			<?php else : ?>
			<?php endif; //end-count ?>

			<?
			} //end-select
			?>

		<?php endwhile; ?>
		<?php wp_reset_query(); ?>
	    </ul>
    </div>
    

	<div class="news">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-11 col-md-12 col-xs-12">
				<h2>Meer nieuws</h2><h5 class="newsLink"><a href="/nieuws">Ga naar nieuwspagina</a></h5>
			</div>
			
				
					<div id="owl-example" class="owl-carousel col-lg-offset-1 col-lg-10 col-md-12 col-xs-12 newsItems">


				<?php $count = 0; ?>
				<?php
				$displayposts = new WP_Query();
				$displayposts->query('post');
				while ($displayposts->have_posts()) : $displayposts->the_post(); ?>
				<?php $count++; ?>
				<?php if ($count<9) : ?>
					<a href="<?php the_permalink(); ?>">
						<div class="newsItem">
							<?php if(get_field('hoofdafbeelding')): ?>
								<div class="newsItemImage <?php the_field('uitlijnen_afbeelding'); ?>" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
							<?php else: ?>
							
								<?php if (in_category('jeugd') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('sponsoren') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('senioren') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('1e-selectie') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('vrijwilligers') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('wedstrijdprogrammas') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('interactief') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('algemeen') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);"></div>
								<?php endif; ?>
								
							<?php endif; ?> 
								<h3 class="newsTitle"><?php echo get_the_title(); ?></h3>
								<h6 class="newsDate"><?php the_time( get_option( 'date_format' ) ); ?></h6>
						</div>
					</a>
					
				<?php else : ?>
				<?php endif; ?>
				<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_query(); ?>
				</div>
			
			
		
			
		</div>
	</div>
	
	<div class="teams">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-4 col-md-5 col-sm-12  TeamButtons">
				<h2>Quick links</h2><br><br><br>
				<div class="col-md-12 col-sm-6 col-xs-12 colTeams">
					<h4>Actueel</h4>
					
						<div class="col-xs-6 colTeamButtons">
							<?php
							if( have_rows('team_vrouwen_links') ):
							while ( have_rows('team_vrouwen_links') ) : the_row(); ?>
								<a href="<?php the_sub_field('link'); ?>"><button class="col-md-12 btn btn-team"><?php the_sub_field('teams'); ?></button></a>
							<?php
							endwhile;
							else :
							endif;
   							?>
						</div>
						<div class="col-xs-6 colTeamButtons colTeamButtons2">
							<?php
							if( have_rows('team_vrouwen_rechts') ):
							while ( have_rows('team_vrouwen_rechts') ) : the_row(); ?>
								<a href="<?php the_sub_field('link'); ?>"><button class="col-md-12 btn btn-team"><?php the_sub_field('teams'); ?></button></a>
							<?php
							endwhile;
							else :
							endif;
   							?>
						</div>
				</div>
				<div class="col-md-12 col-sm-6 col-xs-12 colTeams">
					<h4>Teams</h4>
					
						<div class="col-xs-6 colTeamButtons">
							<?php
							if( have_rows('team_mannen_links') ):
							while ( have_rows('team_mannen_links') ) : the_row(); ?>
								<a href="<?php the_sub_field('link'); ?>"><button class="col-md-12 btn btn-team"><?php the_sub_field('teams'); ?></button></a>
							<?php
							endwhile;
							else :
							endif;
   							?>
						</div>
						<div class="col-xs-6 colTeamButtons colTeamButtons2">
							<?php
							if( have_rows('team_mannen_rechts') ):
							while ( have_rows('team_mannen_rechts') ) : the_row(); ?>
								<a href="<?php the_sub_field('link'); ?>"><button class="col-md-12 btn btn-team"><?php the_sub_field('teams'); ?></button></a>
							<?php
							endwhile;
							else :
							endif;
   							?>
						</div>
				</div>
			</div>
			
			
			<div class="col-md-3 col-xs-12 NextMatchBox">
				<div class="row">
					<div class="col-xs-12">
						<h2><a href="/teams/senioren/?id=161812">1e selectie</a></h2>
					</div>
				</div>
				<div class="col-md-12 NextMatch">
					<h5>Competitie</h5><br>
					<p><span class="NextMatchDate"></span> - <span class="NextMatchTime"></span> uur<br/></p>
					<div class="NextMatchLine"></div>
					<div class="col-xs-6 ClubNextMatch HomeTeam">
						<div class="row">
							<div class="col-xs-12 ClubLogo"></div>
							<div class="col-xs-12 ClubTeamName"></div>
						</div>
					</div>
					<div class="col-xs-6 ClubNextMatch AwayTeam">
						<div class="row">
							<div class="col-xs-12 ClubLogo"></div>
							<div class="col-xs-12 ClubTeamName"></div>
						</div>
					</div>
				</div>
				<div class="col-md-12 Standings">
					<h5 class="classment-title">Stand</h5><br>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>

					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
					<h5 class="AllTeams"><a href="/teams/senioren/?id=161812">Klik hier voor meer info</a></h5>
				</div>
			</div>
			
			<div class="col-md-3 col-xs-12">
				<h2>Quick tv</h2>
				<div class="col-xs-12 VideoSmall VideoSmallFirst">
						<?php the_field('video_1'); ?>
						<h5><?php the_field('video_titel_1'); ?></h5>
				</div>
				<div class="col-xs-12 VideoSmall">
						<?php the_field('video_2'); ?>
						<h5><?php the_field('video_titel_2'); ?></h5>
				</div>
				<h5 class="AllTeams MoreQuickTV"><a href="https://www.youtube.com/user/Quick20TV" target="_blank">Meer Quick'20 tv ></a></h5>
			</div>
		</div>
	</div>
	
	<div class="slogan-agenda">
		<div class="container">
			<div class="col-lg-offset-1 col-md-4 col-xs-12 QuickSlogan">
				
				
				<?php if(get_field('quick_slogan')): ?>
				
				<p>QUICK-SLOGAN</p>
				<div class="slogan"><?php the_field('quick_slogan'); ?></div>
				
				
					
				<?php else: ?>
				<p><br></p>
				<div class="slogan">
					<!-- <a href="<?php the_field('link_afbeelding_slogan'); ?>" target="_blank"><img src="<?php the_field('afbeelding_slogan'); ?>"></a> -->
				</div>
				<p><br></p>
				
				
				<?php endif; ?>
			</div>

<a href="/agenda"><h2>Agenda</h2></a>
	<div class="col-lg-6 col-md-8 col-xs-12 agenda">
<?php
$args = array (
    'post_type' => 'kalender',
    'posts_per_page' => '-1',
    'meta_key' => 'datum',
    'orderby' => 'meta_value',
    'order' => 'ASC'
);
$months = ['', 'januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];
$daysOfTheWeek = ['', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag', 'zondag'];

$the_query = new WP_Query( $args );
?>
<?php if( $the_query->have_posts() ): ?>
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	<?php
		$now = time();
		$tomorrow = $now+86400;
		$gisteren = $now-86400;
		$post_date = get_field('datum');
		$post_timestamp = strtotime($post_date);
		$dateString  = $daysOfTheWeek[date('N', $post_timestamp)];
		$dateString .= ' '.date('j', $post_timestamp);
		$dateString .= ' '.$months[date('n', $post_timestamp)];
		?>
		<?php if($gisteren < $post_timestamp) : ?>
    		<div class="AgendaPoint col-xs-12">
				<div class="col-sm-4 col-xs-5 AgendaDate"><?= $dateString; ?></div>
				<div class="col-sm-2  AgendaTime"><?php the_field('tijd'); ?></div>
				<div class="col-sm-6 col-xs-7 AgendaTitle"><?php echo get_the_title(); ?></div>
			</div>
    	<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); 
?>



					</div>
		</div>
	</div>
	
	<div class="advertising">
		<div class="container">
			<a href="http://www.contict.nl/voetbaltafel" target="_blank">
			<div class="row row-adverts">
				<div class="screen">
					<div class="col-lg-offset-1 col-lg-7 Banner1" style="background-image: url(<?php the_field('banner_groot'); ?>)"></div>
				</div>
				<div class="mobile">
					<div class="col-lg-offset-1 col-lg-7 Banner1" style="background-image: url(<?php the_field('banner_klein'); ?>)"></div>
				</div>
			</div>
			</a>
		</div>
	</div>
	
	<div class="sponsors">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-11 col-xs-12">
				<h2>Sponsoren</h2>
			</div>
			<div class="col-lg-offset-1 col-lg-6 col-md-8 col-xs-12 hoofdsponsors">
				<div class="siers"><a target="_blank" href="<?php the_field('link_hoofdsponsor', 'option'); ?>"><img src="<?php the_field('hoofdsponsor', 'option'); ?>"></a></div>
				<div class="stersponsors">
					<a target="_blank" href="<?php the_field('link_partner_1', 'option'); ?>"><img src="<?php the_field('stersponsor_1', 'option'); ?>"></a>
					<a target="_blank" href="<?php the_field('link_partner_2', 'option'); ?>"><img src="<?php the_field('stersponsor_2', 'option'); ?>"></a>
					<a target="_blank" href="<?php the_field('link_partner_3', 'option'); ?>"><img src="<?php the_field('stersponsor_3', 'option'); ?>"></a>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 SponsorSlider">
				<ul id="scroller">
					<?php
					if( have_rows('logos_sponsoren', 'option') ):
					while ( have_rows('logos_sponsoren', 'option') ) : the_row(); ?>
						<li><a href="<?php the_sub_field('link_sponsor', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('sponsor_logo', 'option'); ?>"></a></li>
					<?php
					endwhile;
					else :
					endif;
   					?>

				</ul>  
			</div>
		</div>
	</div>
	
<?php include 'footer.php';?>