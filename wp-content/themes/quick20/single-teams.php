<?php
/*
Template Name Posts: Teams
Template Name: Teams
*/
?>
<?php get_header(); ?>
	<link href="<?php bloginfo('template_directory'); ?>/css/switchery.css" rel="stylesheet">	
	<?php
		$knvbId = $_GET['id'];
		$staffStr = "'Ass.-trainer/coach', 'Hoofdcoach', 'Teammanager', 'Trainer', 'Trainer/coach', 'Verzorger', 'Verzorger', 'Assistent-scheidsrechter(club)', 'Standaard functie', 'Keeperstrainer'";

	?>
	<link href="<?php bloginfo('template_directory'); ?>/css/switchery.css" rel="stylesheet">	
	<?php
		global $wpdb;
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $knvbId), ARRAY_A);
		$playersFromDb = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function FROM webservice__user u WHERE u.team = %d AND function NOT IN (" . $staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix, first_name", $knvbId ), ARRAY_A);
		$playersPerPos = array();
		foreach($playersFromDb as $player) {
			$pos = $player["function"];
			$pos = $pos == null || $pos == '' ? "rest" : $pos;
			$posArr = $playersPerPos[$pos];
			if ($posArr == null)
				$posArr = array();
			$posArr[] = $player;
			$playersPerPos[$pos] = $posArr;
		}
		$players = array();
		if (is_array($playersPerPos["Doelverdediger"]))
			foreach($playersPerPos["Doelverdediger"] as $player)
				$players[] = $player;
		if (is_array($playersPerPos["Verdediger"]))
			foreach($playersPerPos["Verdediger"] as $player)
				$players[] = $player;
		if (is_array($playersPerPos["Middenvelder"]))
			foreach($playersPerPos["Middenvelder"] as $player)
				$players[] = $player;
		if (is_array($playersPerPos["Aanvaller"]))
			foreach($playersPerPos["Aanvaller"] as $player)
				$players[] = $player;
		if (is_array($playersPerPos["rest"]))
			foreach($playersPerPos["rest"] as $player)
				$players[] = $player;
		$picture = $wpdb->get_var($wpdb->prepare("SELECT value FROM webservice__team_data WHERE team_id = %d AND `key` = 'teampicture'", $knvbId ));
		$staffDb = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function, email, telephone, visibility FROM webservice__user u WHERE u.team = %d AND function IN (" . $staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix, first_name", $knvbId ), ARRAY_A);
		$staff = array();
		foreach($staffDb as $s) {
			if ($s["visibility"] != "Normaal") {
				$s["email"] = null;
				$s["telephone"] = null;
			}
			$staff[] = $s; 
		}
		$newsQuery = $team["short_name"];
		$newsFallbackImage = "jeugd";
		if ($team["category"] == "Senioren") {
			$newsQuery = $team["group"] . $team["short_name"];
			if ($team["group"] == "ZO" && $team["short_name"] == 1)
				$newsFallbackImage = "1e_selectie";
			else
				$newsFallbackImage = "senioren";
		}
		$displayposts = get_posts(['post_type' => 'post', 'posts_per_page' => 32, 'category_name' => $newsQuery]);
		$isFabri = $team["group"] != null && $team["group"] == 'Fabri';
	?>
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title TeamPage"><?php if($isFabri) echo $team["short_name"]; else echo $team["name"]; ?></h1>
		    	<div class="Subtitle TeamPage"><?php echo $team["competition_title"]; ?></span></div>
		    </div>
	    </div>
    </div>
    
	<div class="TeamSingle">
		<div class="container">
			<input type="hidden" id="regCompPouleCode"/>
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<div class="row">
					<div class="col-md-3 col-xs-12 TeamSidebar">
						<ul class="ContentSwitch">
							<li class="SwitchItem active" value="Overview">Teaminfo</li>
							<?php if (sizeof($displayposts) > 0) { ?>
								<li class="SwitchItem" value="News">Nieuws</li>
							<?php } ?>
							<li class="SwitchItem" value="Schedule">Programma</li>
							<li class="SwitchItem" value="Results">Uitslagen</li>
							<li class="SwitchItem" value="Standings">Stand</li>
							<li class="SwitchItem" value="Cup">Beker</li>
							<li class="SwitchItem" value="Playoffs">Nacompetitie</li>
						</ul>
						
						<?php if( have_rows('logos_jeugdgroepen', 'option') ):
							while ( have_rows('logos_jeugdgroepen', 'option') ) : the_row(); 
								if ( get_sub_field('groepsnaam', 'option') == $team["group"]) { ?>
							<div class="row TeamSponsor">
								<div class="col-xs-12">
									<h2 class="black">Stersponsor</h2>
								</div>
								
									<a href="<?php the_sub_field('link_sponsor', 'option'); ?>" target="_blank" >
										<img src="<?php the_sub_field('logo_sponsor', 'option'); ?>">
									</a>
							</div>
						<?php }
						endwhile;
						else :
						endif;
	   					?>
					</div>
					<div class="col-md-9 col-xs-12 TeamContent">
					<!-- Tab teaminfo -->
						<div class="Content Overview">
							<a name="Overview"></a>
							<?php if($picture != null): ?>
								<div id="teampicture" style="display: block;" class="row">
									<img style="width: 100%;" src="/wp-content/uploads/TeamPictures/<?php echo $picture ?>">
								</div>
							<?php endif; ?>
							<div class="row OverviewContent">
								<div class="col-md-7 col-xs-12">
									
									<div class="row TeamRowTitle2 TrainingHeader">
										<div class="col-xs-12">
											<h2 class="black">Trainingstijden</h2>
										</div>
									</div>
									<div class="TeamPlayers TrainingData">
									</div>
									
									<div class="row TeamRowTitle2 TitleSub">
										<div class="col-xs-12">
											<h2 class="black">Team</h2>
										</div>
									</div>
									<div class="TeamPlayers">
										<?php foreach ($players as $player) { ?>
										<div class="row TeamRowPlayer">
											<div class="col-md-7 col-xs-6 PlayerName"><?php echo $player["first_name"] . " " . $player["infix"] . " " . $player["last_name"] ?></div>
											<div class="col-md-5 col-xs-6 PlayerPosition"><?php echo $player["function"] ?></div>
										</div>
										<?php } ?>
									</div>
									<div class="row TeamRowTitle2 TitleSub">
										<div class="col-xs-12">
											<h2 class="black">Staf</h2>
										</div>
									</div>
									<div class="TeamPlayers">
									<?php foreach ($staff as $st) { ?>
										<div class="row TeamRowStaff">
											<div class="col-md-7 PlayerName"><?php echo $st["first_name"] . " " . $st["infix"] . " " . $st["last_name"]; ?></div>
											<div class="col-md-5 PlayerPosition"><?php echo $st["function"] ?></div>
										</div>
										<div class="row TeamRowStaff">
											<div class="col-md-7"><a href="mailto:<?php echo $st["email"] ?>"></a><?php echo $st["email"] ?></div>
											<div class="col-md-5 PlayerPhone"><?php echo $st["telephone"] ?></div>
										</div>
									<?php } ?>
									</div>
								</div>
								<?php $infoPosts = get_posts(['post_type' => 'post', 'posts_per_page' => 3, 'category_name' => $newsQuery]);
								if (sizeof($infoPosts) >= 1): ?>
								<div class="col-md-5 col-xs-12 TeamLatestNews">
									<div class="row">
										<div class="col-xs-12">
											<h2 class="black">Laatste Nieuws</h2>
										</div>
									</div>
									<?php foreach($infoPosts as $post) : ?>
										<a href="<?php the_permalink(); ?>">
										<div class="row TeamNews">
											<?php if(get_field('hoofdafbeelding')): ?>
												<div class="TeamInfoImageSmall" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
											<?php else: ?>
												<div class="TeamInfoImageSmall" style="background-image: url(<?php the_field($newsFallbackImage, 'option'); ?>);"></div>
											<?php endif; ?>
											<div class="TeamNewsTitle"><?php echo get_the_title(); ?></div>
										</div>
										</a>
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
					<!-- Tab Nieuws -->
						<div class="Content News">
							<a name="News"></a>
							<div class="row TeamRowTitle2">
								<div class="col-xs-12">
									<h2 class="black">Nieuws</h2>
								</div>
								<div class="col-xs-12 NewsMessages">
									<div class="row row-news">
										<?php if (sizeof($displayposts) > 0) {
											$count = 0;
											foreach($displayposts as $post) : ?>
											<?php $count++; ?>
												<?php if ($count < 13) : ?>
													<div class="col-md-6 col-left col-right Nieuwsbericht <?php $category = get_the_category($post->ID); echo $category[0]->slug; ?>">
														<a href="<?php the_permalink(); ?>">
															<div class="col-md-12 NewsMessage ">
																<?php if(get_field('hoofdafbeelding')): ?>
																	<div class="col-xs-12 NewsImageSmall" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
																<?php else: ?>
																	<div class="col-xs-12 NewsImageSmall" style="background-image: url(<?php the_field($newsFallbackImage, 'option'); ?>);"></div>
																<?php endif; ?>
																<h3 class="newsTitle"><?php echo get_the_title(); ?></h3>
																<h6 class="newsDate"><?php the_time( get_option( 'date_format' ) ); ?></h6>
															</div>
														</a>
													</div>
												<?php endif; ?>
											<?php endforeach; 
										} ?>
									</div>
								</div>
							</div>
						</div>
					<!-- Tab Programma -->
						<div class="Content Schedule">
							<a name="Schedule"></a>
							<div class="row TeamRowTitle">
								<div class="col-xs-12">
									<div class="row">
										<h2 class="black">Programma</h2>
										<div class="WholeSchedule">
											Volledig programma: <input type="checkbox" class="js-switch schedule" value=""/> 
										</div>
									</div>
									<div id="TeamSchedule" class="TeamSchedule">
										<div class="LoadingIcon Schedule"></div>
										<div id="schedule" class="row Template">
											<h3 class="RowTitle">Programma week </h3>
											<table id="scheduleTable" class="KnvbTable ScheduleTable">
												<tr class="TitleRow">
													<th>datum</th>
													<th>tijd</th>
													<th>thuisploeg</th>
													<th>uitploeg</th>
												</tr>
											</table>
										</div>
									</div>
									<div id="TeamScheduleWhole" class="TeamScheduleWhole" loaded="false">
										<div class="LoadingIcon ScheduleWhole"></div>
										<div class="row Template">
											<h3 class="RowTitle">Programma week </h3>
											<table id="scheduleTable" class="KnvbTable ScheduleTable">
												<tr class="TitleRow">
													<th>datum</th>
													<th>tijd</th>
													<th>thuisploeg</th>
													<th>uitploeg</th>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					<!-- Tab Uitslagen -->
						<div class="Content Results">
							<a name="Results"></a>
							<div class="row TeamRowTitle">
								<div class="col-xs-12">
									<div class="row">
										<h2 class="black">Uitslagen</h2>
										<div class="WholeResults">
											Alle uitslagen: <input type="checkbox" class="js-switch results" value=""/> 
										</div>
									</div>
									<div id="TeamResults" class="TeamResults">
										<div class="row">
											<table id="resultsTable" class="KnvbTable ResultsTable">
												<tr class="TitleRow">
													<th>datum</th>
													<th>tijd</th>
													<th>thuisploeg</th>
													<th>uitploeg</th>
													<th>uitslag</th>
												</tr>
											</table>
										</div>
										<div class="LoadingIcon Results"></div>
									</div>
									<div id="TeamResultsWhole" class="TeamResultsWhole" loaded="false">
										<div class="LoadingIcon TeamResultsWhole"></div>
										<div class="row Template">
											<h3 class="RowTitle">Uitslagen week </h3>
											<table id="resultsTable" class="KnvbTable ResultsTable">
												<tr class="TitleRow">
													<th>datum</th>
													<th>tijd</th>
													<th>thuisploeg</th>
													<th>uitploeg</th>
													<th>uitslag</th>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					<!-- Tab Stand -->
						<div class="Content Standings">
							<a name="Standings"></a>
							<div class="row TeamRowTitle">
								<div class="col-xs-12">
									<div class="row"><h2 class="black">Stand</h2></div>
									<div class="PeriodSwitch"></div>
									<div class="PeriodSwitch Mobile"></div>
									<div id="standings" class="row standings">
										<table class="KnvbTable StandingsTable Template">
											<tr class="TitleRow">
												<th>#</th>
												<th>Team</th>
												<th>G</th>
												<th>W</th>
												<th>GL</th>
												<th>V</th>
												<th>Pnt</th>
												<th>DS</th>
												<th>PM</th>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
					<!-- Tab beker -->
						<div class="Content Cup">
							<a name="Cup"></a>
							<div class="row TeamRowTitle">
								<div class="col-xs-12">
									<div class="row"><h2 class="black">Beker</h2></div>
									<div class="LoadingIcon CupPage"></div>
									<div class="FallBack"><?php echo $team["name"] ?> neemt momenteel niet deel aan de bekercompetitie.</div>
									<div class="CupPage">
									</div>
								</div>
							</div>
						</div>
					<!-- Tab nacompetitie -->
						<div class="Content Playoffs">
							<a name="Playoffs"></a>
							<div class="row TeamRowTitle">
								<div class="col-xs-12">
									<div class="row"><h2 class="black">Nacompetitie</h2></div>
									<div class="LoadingIcon PlayoffsPage"></div>
									<div class="PlayoffsPage">
									</div>
									<div class="FallBack"><?php echo $team["name"] ?> neemt momenteel niet deel aan de nacompetitie.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include 'footer.php';?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.teams.20190607.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/switchery.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_VWcdRmV7LIC90v34_oQdnS_HUdbbqUA"></script>
	<?php
		// 
	?>