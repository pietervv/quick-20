<footer>
<div class="modal fade" id="myModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4>Meld hier je aan voor de Quick'20 Nieuwsbrief!</h4>
      </div>
      <div class="modal-body">


		  <!-- Begin MailChimp Signup Form -->
		  <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
		  <style type="text/css">
			  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
			  /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                 We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
		  </style>
		  <div id="mc_embed_signup">
			  <form action="//quick20.us12.list-manage.com/subscribe/post?u=f9046565266a151d62eed4417&amp;id=9597283c2c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				  <div id="mc_embed_signup_scroll">


					  <div class="mc-field-group">
						  <input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Naam">
					  </div>
					  <div class="mc-field-group">
						  <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="E-mailadres">
					  </div>
					  <div class="mc-field-group">
						  <div class="checkbox">
							  <label>
								  <input type="checkbox" value="subscribe"> Bevestig inschrijving
							  </label>
						  </div>
					  </div>
					  <div id="mce-responses" class="clear">
						  <div class="response" id="mce-error-response" style="display:none"></div>
						  <div class="response" id="mce-success-response" style="display:none"></div>
					  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					  <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f9046565266a151d62eed4417_9597283c2c" tabindex="-1" value=""></div>
					  <div class="clear modal-footer">
						  <input type="submit" value="Inschrijven" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-newsletter">
					  </div>
				  </div>
			  </form>
		  </div>

		  <!--End mc_embed_signup-->

	  </div>
    </div>
  </div>
</div>
		<div class="container">
			<div class="col-lg-offset-1 col-lg-4 col-md-4 col-xs-12 col-sm-4 contact">
				<h5>Bezoekadres</h5><br>
				<p><?php the_field('naam_sportpark', 'option'); ?></p>
				<p><?php the_field('adres', 'option'); ?></p>
				<p><?php the_field('postcode', 'option'); ?> <?php the_field('plaats', 'option'); ?></p>
				<p>Telefoon:
					<a class="tel" tabIndex="-1" href="tel:<?php the_field('telefoon_secretariaat', 'option'); ?>">
						<?php the_field('telefoon_secretariaat', 'option'); ?>
					</a>
				</p>
				<br>
				<h5 class="newsletter"><u data-toggle="modal" data-target="#myModal" >Schrijf je in</u> voor onze nieuwsbrief!</h5>
				<br><br>
				<h5>Social Media</h5>
				<br><br>
				<div class="socialmedia-icon">
					<a target="_blank" href="<?php the_field('facebook_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/facebook-icon.png"></a>
					<a target="_blank" href="<?php the_field('twitter_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/twitter-icon.png"></a>
					<a target="_blank" href="<?php the_field('youtube_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/youtube-icon.png"></a>
					<a target="_blank" href="<?php the_field('instagram_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/instagram-icon.png"></a>
					<a target="_blank" href="<?php the_field('flikr_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/flikr-icon.png"></a>
					<a target="_blank" href="<?php the_field('linkedin_link', 'option'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/linkedin-icon.png"></a>
				</div>
			</div>
			<div class="col-lg-6 col-md-8 col-sm-8 map">
				<h2>maps</h2>
				<div class="LinkRoute">
					<a href="/wp-content/uploads/2016/01/Routebeschrijvingen-Quick20-sportpark.pdf">Klik hier voor uitgebreide routebeschrijving</a>
				</div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1219.5986539043572!2d6.9172529239711436!3d52.31242209999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b8174bfa5557cd%3A0x22c81858a7e7ed17!2sK.V.V.+Quick+&#39;20!5e0!3m2!1snl!2snl!4v1451993583114" width="558" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</footer>
	
	
	<div class="disclaimer">
		<div class="container">
			<div class="col-lg-offset-1 col-md-10">
				<p>
					<a href="/sitemap">Sitemap</a><!-- & <a href="#">Algemene Voorwaarden--></a>
					<span class="footer-link">Ontwerp: <a href="//www.burobam.nl/" title="Brandcube Digital Creation"  target="_blank">buro BAM!</a> | Technische realisatie: <a href="//www.brandcube.nl/" target="_blank">Brandcube</a></span>
				</p>
			</div>
		</div>
	</div>

    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.simplyscroll.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/unslider.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
    

  </body>
</html>