<?php
/*
Template Name: 404 Page Not Found
*/
?>
<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">404!</h1>
		    </div>
	    </div>
    </div>
    
    <div class="viernulvier">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<h1>Sorry, deze pagina bestaat niet.</h1>
				<div class="row"><br>
				<a href="/"><h2>Keer terug naar de Homepagina</h2></a>
				</div>
			</div>
		</div>
    </div>  
	
		