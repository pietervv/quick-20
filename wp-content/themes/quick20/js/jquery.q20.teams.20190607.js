jQuery(document).ready(function($){

/***
	team
***/
	var gameTable = function(tableRef, games, addResult, teamId) {
		if (games != null) {
			displayGameTable(games, tableRef, addResult, teamId);
		} else {
			$(tableRef).append("Geen wedstrijden gevonden.");
		}
	};
	
	var displayGameTable = function(games, tableRef, addResult, teamId) {
		if (games != null && games.length > 0) {
			var rows = "";
			for (var i = 0; i < games.length; i++) {
				var game = games[i];
				var matchId = game["wedstrijdcode"];
				var currentTeam = teamId == game["thuisteamid"] || teamId == game["uitteamid"];
				var penalties = game["PuntenTeam1Strafsch"];
				var penText = null;
				var winner = null;
				if (penalties != null) {
					var team1Pen = penalties;
					var team2Pen = game["PuntenTeam2Strafsch"];
					winner = game["ThuisClub"];
					var winnerEsc = game["ThuisClub"].replace(/\'/g, '&apos;');
					if (team2Pen > team1Pen) {
						winnerEsc = game["UitClub"].replace(/\'/g, '&apos;');
						winner = game["UitClub"];
					}
					var penText = winnerEsc + " wint na strafschoppen (" + team1Pen + " - " + team2Pen + ")";
				}
				rows += "<tr class='DataRow MatchRow";
				if (game["thuisteamid"] == teamId || game["uitteamid"] == teamId)
					rows += " Quick";
				rows += "' data-toggle='modal' data-target='#MatchModal' data-matchid='";
				rows += game["wedstrijdnummer"] + "'";
				if (penText != null)
					rows += " title='" + penText + "'";
				if (currentTeam)
					rows += " class='quick'";
				rows += "><td class='one'>" + game["datumopgemaakt"] + "</td>";
				rows += "<td class='two'>" + game["aanvangstijd"] + "</td>";
				if (winner == game["thuisteam"])
					rows += "<td class='three'>" + game["thuisteam"] + " *</td>";
				else
					rows += "<td class='three'>" + game["thuisteam"] + "</td>";
				if (winner == game["uitteam"])
					rows += "<td class='four'>" + game["thuisteam"] + " *</td>";
				else
					rows += "<td class='four'>" + game["uitteam"] + "</td>";
				if (addResult) {
					rows += "<td class='five'>" + game["uitslag"] + "</td>";
				}
				var icon = currentTeam ? "uploads/images/ball_icon_q.jpg" : "uploads/images/ball_icon.jpg";
				var matchLink = "/index.php?page=match&matchId=" + matchId + "&teamId=" + teamId;
				rows += "</tr>";
			}
			$(tableRef).append(rows);
			$(tableRef + ' TR:even').addClass('zebra');
		} else {
			$(tableRef).append("Geen wedstrijden gevonden.");
		}
	}
	
	var rankingTable = function(tableRef, teams, teamId) {
		if (teams != null && teams.length > 0) {
			var rows = "";
			for (var i = 0; i < teams.length; i++) {
				var team = teams[i];
				var saldo = team["doelpuntenvoor"] - team["doelpuntentegen"];
				var saldoTitle = "Voor: " + team["doelpuntenvoor"] + "&#10;Tegen: " + team["doelpuntentegen"];
				if (saldo > 0)
					saldo = "+" + saldo;
				if (team["clubrelatiecode"] == "BBKT65L")
					rows += "<tr class='DataRow Quick'";
				else
					rows += "<tr class='DataRow'";
				
				var teamStr = team["teamnaam"];
				if (team["AddedInfo"] != null && team["AddedInfo"]["website"] != null && team["AddedInfo"]["website"] != '')
					teamStr = "<a target='_blank' href='" + team["AddedInfo"]["website"] + "'>" + team["naam"] + "</a>";
				rows += "><td class='one'>" + team["positie"] + "</td>";
				var logo = "http://bin617.website-voetbal.nl/sites/voetbal.nl/files/knvblogos_width35/" + team["clubrelatiecode"] + ".png";
				rows += "<td class='two'><img class='ClubLogo' src='" + logo + "'/>" + teamStr + "</td>";
				rows += "<td class='three'>" + team["gespeeldewedstrijden"] + "</td>";
				rows += "<td class='four'>" + team["gewonnen"] + "</td>";
				rows += "<td class='five'>" + team["gelijk"] + "</td>";
				rows += "<td class='six'>" + team["verloren"] + "</td>";
				rows += "<td class='seven'>" + team["punten"] + "</td>";
				rows += "<td class='eight' title='" + saldoTitle + "'>" + saldo + "</td>";
				if (team["verliespunten"] > 0)
					rows += "<td class='nine'>" + team["verliespunten"] + "</td></tr>";
				else
					rows += "<td class='nine'></td></tr>";
			}
			$(tableRef).append(rows);
			$(tableRef + ' TR:even').addClass('zebra');
		} else {
			$(tableRef).append("Geen stand gevonden.");
		}
	};

	var displayRegularCompetition = function(competition, teamId) {
		var overview = competition["details"];
		$("DIV#standings DIV.title").append(" " + overview["klassepoule"]);
		$("DIV#schedule DIV.title").append(" " + overview["klassepoule"]);
		gameTable('DIV.TeamResults TABLE#resultsTable', competition["results"], true, teamId);
		$(".LoadingIcon.Results").remove();
		if (competition["schedule"] == null || competition["schedule"].length == 0) {
			$('DIV.TeamSchedule').append("<br/>Geen wedstrijden gevonden.");
		}
		$.each(competition["schedule"], function(week, schedule) {
			var newScheduleDiv = $("DIV.Schedule DIV.TeamSchedule DIV.Template").clone();
			newScheduleDiv.find(".RowTitle").append(" " + week);
			newScheduleDiv.removeClass("Template");
			newScheduleDiv.find("TABLE.ScheduleTable").addClass("week" + week);
			newScheduleDiv.appendTo("DIV.TeamSchedule");
			gameTable('DIV.TeamSchedule TABLE.week' + week, schedule, false, teamId);
		});
		$(".LoadingIcon.Schedule").remove();
		$("DIV.Schedule DIV.TeamSchedule DIV.Template").remove();
		
		var tableSelector = "DIV.Standings Table.Template";
		var tableContainer = $(tableSelector).parent();
		
		if (competition["hasPeriods"]) {
			// Add the table for the regular season:
			var seasonTable = $(tableSelector).clone();
			seasonTable.addClass("Standard Period PeriodS");
			seasonTable.removeClass("Template");
			seasonTable.show();
			tableContainer.append(seasonTable);
			rankingTable("TABLE.PeriodS", competition["ranking"], teamId);
			
			// Update the links to the periods:
			var switchDiv = "<a class='selected loaded' period='S'>Seizoen</a>";
			var switchDivMob = "<a class='selected loaded' period='S'>Seizoen</a>";
			$.each(competition["periods"], function() {
				// Add links for every period
				var period = this.waarde;
				switchDiv += " | <a period='" + period + "'>Periode " + period + "</a>";
				switchDivMob += " | <a period='" + period + "'>P" + period + "</a>";
				// Add a table (empty until opened):
				var periodTable = $(tableSelector).clone();
				periodTable.removeClass("Standard");
				periodTable.addClass("Period Period" + period);
				periodTable.removeClass("Template");
				tableContainer.append(periodTable);
				$("Period" + period).hide();
			});
			
			
			$("DIV.PeriodSwitch").html(switchDiv);
			$("DIV.PeriodSwitch.Mobile").html(switchDivMob);
			$("DIV.PeriodSwitch A").click(function() {
				var periodLink = $(this);
				var selectedPeriod = $(this).attr("period");
				var isLoaded = $(this).hasClass("loaded");
				if (!isLoaded) {
					var pouleCode = $("#regCompPouleCode").val();
					$.ajax({
						url: urlBase + "/webservice/cdPeriodRanking",
						type: "POST",
						data: { "pouleCode" : pouleCode, "periodNr" : selectedPeriod },
						dataType: "json",
						success: function(data) {
							periodLink.addClass("loaded");
							rankingTable('TABLE.Period' + selectedPeriod, data, teamId);
							$("TABLE.Period").hide();
							$("TABLE.Period.Period" + selectedPeriod).show();
							$("DIV.PeriodSwitch A").removeClass("selected");
							periodLink.addClass("selected");
						}
					});
				}
				$("TABLE.Period").hide();
				$("TABLE.Period.Period" + selectedPeriod).show();
				$("DIV.PeriodSwitch A").removeClass("selected");
				$(this).addClass("selected");
			});
		} else {
			var seasonTable = $(tableSelector).clone();
			seasonTable.addClass("Standard PeriodS");
			seasonTable.removeClass("Template");
			tableContainer.append(seasonTable);
			rankingTable("DIV.Standings TABLE.PeriodS", competition["ranking"], teamId);
		}
		$(".LoadingIcon.Standings").remove();
	};
	
	var displaySpecial = function(competition, teamId, divClassName) {
		var cls = "KnvbTable";
		var div = $("DIV." + divClassName);
		var overview = competition["details"];
		var compId = overview["poulecode"];
		var compName = overview["competitienaam"];
		var schedule = competition["schedule"];
		if (schedule != null && schedule.length > 0) {
			div.append("<h3 class='RowTitle'>" + compName + ": programma</h3>");
			div.append("<TABLE id='schedule" + compId + "'></table>");
			var cupSchedTable = $('TABLE#schedule' + compId);
			cupSchedTable.append("<thead><tr><th>datum</th><th>tijd</th><th>thuisploeg</th><th>uitploeg</th><th></tr></thead>");
			gameTable('TABLE#schedule' + compId, schedule, false, teamId);
			cupSchedTable.addClass(cls);
			cupSchedTable.addClass('ScheduleTable');
		}
		var results = competition["results"];
		if (results != null && results.length > 0) {
			div.append("<h3 class='RowTitle'>" + compName + ": uitslagen</h3>");
			div.append("<TABLE id='results" + compId + "'></table>");
			var cupResTable = $('TABLE#results' + compId);
			cupResTable.append("<thead><tr><th>datum</th><th>tijd</th><th>thuisploeg</th><th>uitploeg</th><th>uitslag</th></tr></thead>");
			gameTable('TABLE#results' + compId, results, true, teamId);
			cupResTable.addClass(cls);
			cupResTable.addClass('ResultsTable');
		}
		var ranking = competition["ranking"]
		if (ranking != null) {
			div.append("<h3 class='RowTitle'>" + compName + ": stand</h3>");
			div.append("<TABLE id='ranking" + compId + "'></table>");
			var cupRankTable = $('TABLE#ranking' + compId);
			cupRankTable.append("<thead><tr><th>#</th><th>Team</th><th>G</th><th>W</th><th>GL</th><th>V</th><th>Pnt</th><th>DS</th><th>PM</th></tr></thead>");
			div.append("</table>");
			rankingTable('TABLE#ranking' + compId , ranking, teamId);
			cupRankTable.addClass(cls);
			cupRankTable.addClass('StandingsTable');
		}
		$(".LoadingIcon." + divClassName).remove();
	};
	
	var displayTrainingData = function(trainingData) {
		var container = $("DIV.TrainingData");
		$.each(trainingData, function() {
			var day = getDayOfWeek(this.dayOfWeek);
			var time = this.start + " - " + this.end;
			var row = "<div class='row TeamRowPlayer'>";
			row += "<div class='col-md-5 col-xs-6'>";
			row += day + "<br/>" + time + "</div>";
			row += "<div class='col-md-7 col-xs-6'>";
			row += "Veld " + this.locatie + " kleedkamer " + this.kleedkamer;
			row += "<br/>" + this.opmerkingen + "</div></div>";
			container.append(row);
		});
	};

	$("DIV#TeamSchedule").each(function() {
		var teamId = getUrlParameter("id");
		$.ajax({
			url: urlBase + "/webservice/cdCompetition",
			type: "POST",
			data: { "teamcode" : teamId },
			dataType: "json",
			success: function(data) {
				var regCompPouleCode = data["regCompPouleCode"];
				$("#regCompPouleCode").val(regCompPouleCode);
				var competitions = data["competitions"];
				var cupList = [];
				var playoffList = [];
				var compList = [];
				if (competitions != null) {
					for (var count = 0; count < competitions.length; count++) {
						var competition = competitions[count];
						var details = competition["details"];
						var type = details["competitiesoort"];
						if (type == 'beker') {
							var id = competition["details"]["poulecode"] + "";
							var cupMap = {};
							cupMap["id"] = id;
							cupMap["comp"] = competition;
							cupList.push(cupMap);
						}  else if (type=='regulier') {
							var id = competition["details"]["poulecode"] + "";
							var compMap = {};
							compMap["id"] = id;
							compMap["comp"] = competition;
							compList.push(compMap);
							
						} else if (type == 'nacompetitie') {
							var id = competition["details"]["poulecode"] + "";
							var playoffMap = {};
							playoffMap["id"] = id;
							playoffMap["comp"] = competition;
							playoffList.push(playoffMap);
						}
					}
					if (!jQuery.isEmptyObject(cupList)) {
						cupList = cupList.sort(function (a, b) {
							return b.id.localeCompare(a.id);
						});
						for (var i = 0; i < cupList.length; i++) {
							displaySpecial(cupList[i].comp, teamId, "CupPage");
						}
					} else {
						$("DIV.Cup DIV.FallBack").show();
						$(".LoadingIcon.CupPage").remove();
					}
					if (!jQuery.isEmptyObject(playoffList)) {
						playoffList = playoffList.sort(function (a, b) {
							return b.id.localeCompare(a.id);
						});
						for (var i = 0; i < playoffList.length; i++) {
							displaySpecial(playoffList[i].comp, teamId, "PlayoffsPage");
						}
					} else {
						$("DIV.Playoffs DIV.FallBack").show();
						$(".LoadingIcon.PlayoffsPage").remove();
					}
					if (!jQuery.isEmptyObject(compList)) {
						compList = compList.sort(function (a, b) {
							return b.id.localeCompare(a.id);
						});
						displayRegularCompetition(compList[0]["comp"], teamId);
					}
				}
				// Training:
				var trainingData = data["trainingData"];
				if (!jQuery.isEmptyObject(trainingData)) {
					displayTrainingData(trainingData);
				}
			}
		});
		var ref = getUrlParameter("ref");
		$.ajax({
			url: urlBase + "/webservice/check",
			type: "POST",
			data: { "teamId" : teamId, "sub" : "init/" + ref },
			dataType: "json"
		});
	});
	
	// Whole season switches:
	var elemSch = document.querySelector('.js-switch.schedule');
	var init = new Switchery(elemSch);
	
	var isDraggingSchedule = false;
	$('.WholeSchedule .switchery').mousedown(function() {
		isDraggingSchedule = false;
	});
	$('.WholeSchedule .switchery').mousemove(function() {
		isDraggingSchedule = true;
	});
	$('.WholeSchedule .switchery').mouseup(function() {
		var wasDragging = isDraggingSchedule;
		isDraggingSchedule = false;
		if (wasDragging)
			$('.js-switch.schedule').trigger("click");
	});
	$('.WholeSchedule .switchery').bind( "touchstart", function(e) {
		isDraggingSchedule = false;
	});
	$('.WholeSchedule .switchery').bind( "touchmove", function(e) {
		isDraggingSchedule = true;
	});
	$('.WholeSchedule .switchery').bind( "touchend", function(e) {
		var wasDragging = isDraggingSchedule;
		isDraggingSchedule = false;
		if (wasDragging)
			$('.js-switch.schedule').trigger("click");
	});
	
	$('.js-switch.schedule').change(function() {
		var teamId = getUrlParameter("id");
		var pouleCode = $("#regCompPouleCode").val();
		$("DIV#TeamSchedule").toggle();
		$("DIV#TeamScheduleWhole").toggle();
		var isLoaded = $("DIV#TeamScheduleWhole").attr("loaded");
		if (isLoaded == "false") {
			$("DIV#TeamScheduleWhole").attr("loaded", "true");
			$.ajax({
				url: urlBase + "/webservice/cdScheduleAll",
				type: "POST",
				data: { "teamId" : teamId, "pouleCode" : pouleCode },
				dataType: "json",
				success: function(data) {
					$.each(data, function(weekKey, schedule) {
						var newScheduleDiv = $("DIV.Schedule DIV.TeamScheduleWhole DIV.Template").clone();
						var weekText = weekKey.substring(4, 7) + " " + weekKey.substring(0, 4);
						newScheduleDiv.find(".RowTitle").append(weekText);
						newScheduleDiv.removeClass("Template");
						newScheduleDiv.find("TABLE.ScheduleTable").addClass("week" + weekKey);
						newScheduleDiv.appendTo("DIV.TeamScheduleWhole");
						displayGameTable(schedule, 'DIV.TeamScheduleWhole TABLE.week' + weekKey, false, teamId);
					});
					$("DIV.TeamScheduleWhole DIV.Template").remove();
					$(".LoadingIcon.ScheduleWhole").remove();
				}
			});
		}
		$.ajax({
			url: urlBase + "/webservice/check",
			type: "POST",
			data: { "teamId" : teamId, "sub" : "ScheduleWhole" },
			dataType: "json"
		});
	});
	
	var elemRes = document.querySelector('.js-switch.results');
	var init = new Switchery(elemRes);
	var isDraggingResults = false;
	$('.WholeResults .switchery').mousedown(function() {
		isDraggingResults = false;
	});
	$('.WholeResults .switchery').mousemove(function() {
		isDraggingResults = true;
	});
	$('.WholeResults .switchery').mouseup(function() {
		var wasDragging = isDraggingResults;
		isDraggingResults = false;
		if (wasDragging)
			$('.js-switch.results').trigger("click");
	});
	
	$('.WholeResults .switchery').bind( "touchstart", function(e) {
		isDraggingResults = false;
	});
	$('.WholeResults .switchery').bind( "touchmove", function(e) {
		isDraggingResults = true;
	});
	$('.WholeResults .switchery').bind( "touchend", function(e) {
		var wasDragging = isDraggingResults;
		isDraggingResults = false;
		if (wasDragging)
			$('.js-switch.results').trigger("click");
	});
	$('.js-switch.results').change(function() {
		var teamId = getUrlParameter("id");
		var pouleCode = $("#regCompPouleCode").val();
		$("DIV#TeamResults").toggle();
		$("DIV#TeamResultsWhole").toggle();
		var isLoaded = $("DIV#TeamResultsWhole").attr("loaded");
		if (isLoaded == "false") {
			$("DIV#TeamResultsWhole").attr("loaded", "true");
			$.ajax({
				url: urlBase + "/webservice/cdResultsAll",
				type: "POST",
				data: { "teamId" : teamId, "pouleCode" : pouleCode },
				dataType: "json",
				success: function(data) {
					$.each(data, function(weekKey, result) {
						var newResultDiv = $("DIV.Results DIV.TeamResultsWhole DIV.Template").clone();
						var weekText = weekKey.substring(4, 7) + " " + weekKey.substring(0, 4);
						newResultDiv.find(".RowTitle").append(weekText);
						newResultDiv.removeClass("Template");
						newResultDiv.find("TABLE.ResultsTable").addClass("week" + weekKey);
						newResultDiv.appendTo("DIV.TeamResultsWhole");
						displayGameTable(result, 'DIV.TeamResultsWhole TABLE.week' + weekKey, true, teamId);
					});
					$("DIV.TeamResultsWhole DIV.Template").remove();
					$(".LoadingIcon.TeamResultsWhole").remove();
				}
			});
		}
		$.ajax({
			url: urlBase + "/webservice/check",
			type: "POST",
			data: { "teamId" : teamId, "sub" : "ResultsWhole" },
			dataType: "json"
		});
	});
	
	// End whole season switches
});

/***
hulpfuncties
***/
	function initiliazeGmaps() {
		var destLat = $("SPAN#map_lat").attr("value");
		var destLon = $("SPAN#map_long").attr("value");
		var distance = calcDistance(destLat, destLon);
		var zoomLevel = 15;
		if (distance > 50)
			zoomLevel = 11;
		else if (distance > 30)
			zoomLevel = 12;
		else if (distance > 15)
			zoomLevel = 13;
		else if (distance > 5)
			zoomLevel = 14;
		var placeholder = document.getElementById('g_maps');
		var position = new google.maps.LatLng(destLat, destLon);
		var options = {
			center: position,
			zoom: zoomLevel,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(placeholder, options);
		var marker = new google.maps.Marker({
		    position: position,
		    map: map
		});
	}
	
	function calcDistance(destLat, destLong) {
		// Berekent het aantal km hemelsbreed vanaf Q'20
		// Liever vanaf de positie van de gebruiker, maar die weten we niet
		var fromLat = 52.3127891179138;
		var fromLong = 6.91846552011526;
		var radius = 6371;
		var φFrom = fromLat * Math.PI / 180;
		var λFrom = fromLong * Math.PI / 180;
		var φDest = destLat * Math.PI / 180;
		var λDest = destLong * Math.PI / 180;
		var Δφ = φDest - φFrom;
		var Δλ = λDest - λFrom;
		var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) + Math.cos(φFrom) * Math.cos(φDest) *	Math.sin(Δλ/2) * Math.sin(Δλ/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		var d = radius * c;
		return d;
	}
	
	var knvbMap = {};
	knvbMap["ADO"] = "Afgelast door organisatie";
	knvbMap["ADB"] = "Afgelast door bond";
	knvbMap["AFG"] = "Afgelast";
	knvbMap["BNO"] = "Bezoekend team niet opgekomen";
	knvbMap["GOB"] = "Gestaakt wegens onvoldoende spelers uitspeelend team";
	knvbMap["GOT"] = "Gestaakt wegens onvoldoende spelers thuisspelend team";
	knvbMap["GVS"] = "Geen vervangende scheidsrechter";
	knvbMap["GWO"] = "Gestaakt wegens ongeval";
	knvbMap["GWT"] = "Gestaakt wegens tuchtzaak";
	knvbMap["GWW"] = "Gestaakt wegens weersomstandigheden";
	knvbMap["NOB"] = "Niet opgekomen beide teams";
	knvbMap["SNO"] = "Scheidsrechter niet opgekomen";
	knvbMap["TAS"] = "Terreinafkeuring door scheidsrechter";
	knvbMap["TNO"] = "Thuisspelend team niet opgekomen";
	knvbMap["WNO"] = "Wedstrijdformulier niet ontvangen";
	knvbMap["WOV"] = "Wedstrijd ongeldig verklaard";
	
	function knvbErrorText(abbr) {
		var txt = knvbMap[abbr];
		return txt == null ? abbr : txt;
	}
	
	function getDayOfWeek(dayNr) {
		return isNaN(dayNr) ? null : ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'][dayNr];
	}
