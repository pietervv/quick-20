jQuery(document).ready(function($) {
	var displayHistoryRankingTable = function(competition, container) {
		var standingsDiv = $(".Templates .StandingsDiv").clone();
		$(container).prepend(standingsDiv);
		var overview = competition["overview"];
		var matches = competition["matches"];
		var competitionName = overview["name"];
		standingsDiv.find('.StandingsTitle').append("<h3 class='RowTitle Clear'>" + competitionName + "</h3>"); 
		var ranking = overview["ranking"];
		var rankingSplit = ranking.split(',');
		var rows = "";
		$.each(rankingSplit, function() {
			var teamRanking = this;
			if (teamRanking == null || teamRanking == '')
				return true;
			var teamRankingSplit = teamRanking.split(':');
			var rank = teamRankingSplit[0];
			var teamName = teamRankingSplit[1];
			var teamCode = teamRankingSplit[2];
			var teamId = teamRankingSplit[3];
			var teamResultsStr = teamRankingSplit[4];
			// Example: 22.21.1.0.64.127.15.0 = G.W.G.V.P.Dpv.Dpt.Aftrek
			var teamResults = teamResultsStr.split('.');
			rows += "<tr class='DataRow Clear'";
			rows += "><td class='one'>" + rank + "</td>";
			rows += "<td class='two'>" + teamName + "</td>";
			rows += "<td class='three'>" + teamResults[0] + "</td>";
			rows += "<td class='four'>" + teamResults[1] + "</td>";
			rows += "<td class='five'>" + teamResults[2] + "</td>";
			rows += "<td class='six'>" + teamResults[3] + "</td>";
			rows += "<td class='seven'>" + teamResults[4] + "</td>";
			var voor = teamResults[5];
			var tegen = teamResults[6];
			var saldo = parseInt(voor) - parseInt(tegen);
			var saldoTitle = "Voor: " + voor + "&#10;Tegen: " + tegen;
			if (saldo > 0)
				saldo = "+" + saldo;
			rows += "<td class='eight' title='" + saldoTitle + "'>" + saldo + "</td>";
			var aftrek = teamResults[7];
			if (aftrek > 0)
				rows += "<td class='nine'>" + aftrek + "</td></tr>";
			else
				rows += "<td class='nine'></td></tr>";
		});
		var standingsTable = $(standingsDiv).find(".StandingsTable");
		$(standingsDiv).append(standingsTable);
		$(standingsTable).append(rows);
		$(standingsDiv).addClass("Clear");
		$(standingsTable).find('TR:even').addClass('zebra');
	};
	
	var displayHistoryResultsTable = function(competition, container) {
		var overview = competition["overview"];
		var includeRanking = true;//overview["has_ranking"] != null && overview["has_ranking"] == "1";
		var competitionName = overview["name"];
		var resultsContainer = $("<div class='CompetitionContainer Clear'></div>");
		resultsContainer.prependTo($(container));
		resultsContainer.append("<h3 class='RowTitle Clear'>" + competitionName + "</h3>");
		if (includeRanking)
			var rankingMap = createRankingMap(overview["ranking"]);
		var matches = competition["matches"];
		$.each(matches, function(index, matchesPerRound) {
			var round = matchesPerRound["round"];
			if (round == null)
				round = matchesPerRound["date"];
			var resultsStr = matchesPerRound["results"];
			var resultsSplit = resultsStr.split(',');
			var newResultsDiv = $("DIV.Templates DIV.ResultsTable").clone();
			newResultsDiv.find(".RowTitle").append(" " + round);
			newResultsDiv.addClass("Clear");
			newResultsDiv.find("TABLE.ResultsTable").addClass("week" + round);
			resultsContainer.append(newResultsDiv);
			var rows = "";
			$.each(resultsSplit, function() {
				var resultStr = this;
				if (resultStr == null || resultStr == '')
					return true;
				var resultSplit = resultStr.split(':');
				var teams = resultSplit[0];
				var score = resultSplit[1];
				var teamSplit = teams.split('-');
				var homeTeam = teamSplit[0];
				var awayTeam = teamSplit[1];
				if (includeRanking) {
					homeTeam = rankingMap[homeTeam];
					awayTeam = rankingMap[awayTeam];
				}
				rows += "<tr class='DataRow'>";
				rows += "<td class='three'>" + homeTeam + "</td>";
				rows += "<td class='four'>" + awayTeam + "</td>";
				rows += "<td class='five'>" + score + "</td>";
				rows += "</tr>";
			});
			newResultsDiv.find("TABLE.ResultsTable").append(rows);
		});
		if (!includeRanking)
			resultsContainer.find(".RoundTitle").remove();
	};
	
	var createRankingMap = function(ranking) {
		var rankingMap = {};
		var rankingSplit = ranking.split(',');
		$.each(rankingSplit, function() {
			var teamRanking = this;
			if (teamRanking == null || teamRanking == '')
				return true;
			var teamRankingSplit = teamRanking.split(':');
			var rank = teamRankingSplit[0];
			var teamName = teamRankingSplit[1];
			rankingMap[rank.toString()] = teamName;
		});
		return rankingMap;
	};
	
	var resetPage = function() {
		$(".Clear").remove();
	};
	
	$("SELECT.season").on("change", function() {
		$(this).removeClass("UnSelected");
		var selectedSeason = $(this).val();
		var selectedTeam = $("SELECT[name='team']").val();
		$.ajax({
			url: urlBase + "/webservice/detailsTeamSeason",
			type: "POST",
			data: { "season" : selectedSeason, "teamId" : selectedTeam },
			dataType: "json",
			success: function(data) {
				// Delete old data
				resetPage();
				
				// Team details
				var teamDetails = data["team"];
				var teamTitle = teamDetails["team_name"] + " - seizoen " + teamDetails["season"] + " / " + (parseInt(teamDetails["season"]) + 1);
				$(".TeamTitle").append("<h2 class='black Clear'>" + teamTitle + "</h2>");
				
				// Players
				var players = data["players"];
				var tempRow = $(".Templates .TeamRowPlayer");
				$.each(players, function(index, player) {
					var newRow = tempRow.clone();
					newRow.find(".PlayerName").append(player["name"]);
					newRow.find(".PlayerPosition").append(player["function"]);
					newRow.addClass("Clear");
					$(".TeamPlayers").append(newRow);
				});
				
				// Competitions
				var competitions = data["competitions"];
				$.each(competitions, function(index, competition) {
					var overview = competition["overview"];
					var compId = overview["id"];
					var type = overview["type"];
					var name = overview["name"];
					if (type == 'R') {
						displayHistoryRankingTable(competition, '.History .Standings .StandingsContainer');
						displayHistoryResultsTable(competition, '.History .Results .ResultsDiv');
					} else if (type == 'B') {
						var hasRanking = overview["has_ranking"];
						if (hasRanking != null && hasRanking == '1')
							displayHistoryRankingTable(competition, ".History .CupDiv");
						displayHistoryResultsTable(competition, ".History .CupDiv");
					} else if (type == 'N') {
						var hasRanking = overview["has_ranking"];
						if (hasRanking != null && hasRanking == '1')
							displayHistoryRankingTable(competition, ".History .PlayOffsDiv");
						displayHistoryResultsTable(competition, ".History .PlayOffsDiv");
					}
				});
				// Display the stuff
				$(".NotSelected").hide();
				$(".Selected").show();
			}
		});
	});
	
	$("SELECT[name='team']").each(function() {
		$.ajax({
			url: urlBase + "/webservice/allTeams",
			type: "GET",
			dataType: "json",
			success: function(data) {
				var teamSelect = $("SELECT[name='team']");
				var selectHtml = "";
				$.each(data["teams"], function(index, team) {
					selectHtml += "<option value='" + team["knvb_id"] + "'>" + team["team_name"] + "</option>";					
				});
				teamSelect.append(selectHtml);
				$("DIV.team").show();
			}
		});
	});
	
	$("SELECT[name='team']").on("change", function() {
		resetPage();
		$(this).removeClass("UnSelected");
		$("SELECT.season").addClass("UnSelected");
		$("SELECT.season").find("OPTION:not(.initial)").remove();
		$(".NotSelected").show();
		$(".Selected").hide();
		$("SELECT.season").prop('selectedIndex', 0);
		var selectedTeam = $(this).val();
		$.ajax({
			url: urlBase + "/webservice/seasonsForTeam",
			type: "POST",
			data: { "teamId" : selectedTeam },
			dataType: "json",
			success: function(data) {
				var seasonSelect = $("SELECT[name='season']");
				var selectHtml = "";
				$.each(data["seasons"], function(index, season) {
					var season = parseInt(season["season"]);
					selectHtml += "<option value='" + season + "'>" + season + " / " + (season + 1) + "</option>";					
				});
				seasonSelect.append(selectHtml);
			}
		});
	});
});