jQuery(document).ready(function($) {
	$('.CollapsableTitle').click(function() {
		$(this).siblings(".CollapsableContent").each(function() {
			$(this).find("DIV.ExercisesVideo.Init").each(function() {
				var ytCode = $(this).attr("data-id");
				var ifr = "<iframe width='100%' frameborder='1' height='320' allowfullscreen='' src='https://www.youtube.com/embed/" + ytCode + "?&theme=dark&autohide=1&modestbranding=1&showinfo=0&rel=0'></iframe>";
				$(this).append(ifr);
				$(this).removeClass("Init");
			});
			$(this).slideToggle();
		});
		var parent = $(this).parent();
		if (parent.attr("collapsed") == "1") {
			parent.animate({width:80+'%'});
			parent.attr("collapsed", "0");
			parent.addClass("Open");
		} else {
			parent.animate({width:50+'%'});
			parent.attr("collapsed", "1");
			parent.removeClass("Open");
		}
		$(this).find("SPAN").toggleClass("down");
		$(this).find("SPAN").toggleClass("up");
	});
	
	$(".TrainersPage .dropdown-menu LI").each(function(index) {
		var value = $(this).attr("value");
		if (index == 0) {
			$("SPAN.FilterTitle").html(value);
			$(".TrainersPage .OefenvormGroep").each(function() {
				if($(this).attr("value") == value)
					$(this).show();
			});
		}
	});
	
	$('.TrainersPage .dropdown-menu LI').on('click', function(){
		var value = $(this).attr("value");
		$("SPAN.FilterTitle").html(value);
		$(".TrainersPage .OefenvormGroep").each(function() {
			if($(this).attr("value") != value)
				$(this).hide();
			else
				$(this).show();
		});
		$(this).addClass("active");
		$(this).siblings("LI").removeClass("active");
	});
});