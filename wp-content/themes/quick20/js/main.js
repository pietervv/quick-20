//SLIDER
var urlBase = '';

$('.my-slider').unslider({
	animation: 'fade', 
	autoplay: true, 
	arrows: true,
	speed: 1500,
	delay: 6000,
	nav:false 
});
	
	
	
(function($) {
	$(function() { //on DOM ready 
    		$("#scroller").simplyScroll();
	});
 })(jQuery);

//OWL-CAROUSEL
var owl = $('.owl-carousel');
owl.owlCarousel({
    loop:true,
    nav:false,
    margin:30,
    autoplay:true,
	autoplayTimeout: 2000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },            
        960:{
            items:4
        }
    }
});

	 

//TEAM SWITCH
$(document).ready(function(){
	$(".SwitchItem").click(function(){
		var selector = $(this).attr("value");
		$(this).addClass("active");
		$(this).siblings(".SwitchItem").each(function() {
			$(this).removeClass("active");
		});
		$("DIV." + selector).css("display", "block");
		$("DIV." + selector).siblings(".Content").each(function() {
			$(this).css("display", "none");
		});
        //location.hash = "#" + selector;
        $('html, body').animate({scrollTop: $("DIV." + selector).offset().top}, 500);
        var teamId = getUrlParameter("id");
        if (teamId != null && teamId != '') {
	        $.ajax({
				url: urlBase + "/webservice/check",
				type: "POST",
				data: { "teamId" : teamId, "sub" : selector },
				dataType: "json"
			});
		}
    });
    $("DIV.NextMatch").each(function() {
    	var quickId = 161812; // Debug met andere teams: 163601, 96510, 98870, 161971, 120329, 155086, 93197
    	$.ajax({
			url: urlBase + "/webservice/cdTeamSummary",
			type: "POST",
			data: { "teamId" : quickId },
			dataType: "json",
			success: function(data) {
				// Next match
				if(data != null && data["nextMatch"] != null) {
					var game = data["nextMatch"];
	    			$('SPAN.NextMatchDate').append(game["datumopgemaakt"]);
	    			$('SPAN.NextMatchTime').append(game["aanvangstijd"]);
	    			var logoThuis = "http://bin617.website-voetbal.nl/sites/voetbal.nl/files/knvblogos_width35/" + game["thuisteamclubrelatiecode"] + ".png";
	    			var logoUit = "http://bin617.website-voetbal.nl/sites/voetbal.nl/files/knvblogos_width35/" + game["uitteamclubrelatiecode"] + ".png";
	    			$('DIV.ClubNextMatch.HomeTeam DIV.ClubLogo').css('background-image', 'url(' + logoThuis + ')');
	    			$('DIV.ClubNextMatch.AwayTeam DIV.ClubLogo').css('background-image', 'url(' + logoUit + ')');
	    			$('DIV.ClubNextMatch.HomeTeam DIV.ClubTeamName').append(game["thuisteam"]);
	    			$('DIV.ClubNextMatch.AwayTeam DIV.ClubTeamName').append(game["uitteam"]);
				} else {
					//$(".NextMatch").hide();
				}
    			// Ranking
    			if(data["ranking"] != null) {
					var ranking = data["ranking"];
					var nrOfTeams = ranking.length;
					var quickIndex = -1;
					for (var i = 0; i < nrOfTeams; i++) {
						var team = ranking[i];
						if (team["clubrelatiecode"] == "BBKT65L") {
							quickIndex = i;
							break;
						}
	    			}
	    			var teamsToDisplay = [];
	    			var startIndex = quickIndex <= 2 ? 1 : Math.min(nrOfTeams - 3, quickIndex - 1);
	    			teamsToDisplay[0] = ranking[0];
	    			for (var teamIdx = startIndex, idx = 1; teamIdx < startIndex + 3; teamIdx++, idx++)
	    				teamsToDisplay[idx] = ranking[teamIdx];
	    			$("DIV.Standings DIV.standing-row").each(function(index) {
	    				var team = teamsToDisplay[index];
	    				$(this).find("DIV.stand-team").append(team["positie"] + ". " + team["teamnaam"]);
	    				$(this).find("DIV.stand-games").append(team["gespeeldewedstrijden"]);
	    				$(this).find("DIV.stand-points").append(team["punten"]);
	    				if (team["clubrelatiecode"] == "BBKT65L")
	    					$(this).addClass("quick");
	    			});
				} else {
					// TODO: ergens afbeelden: geen stand aanwezig ?
				}
			}
		});
    });
 
});

/*

var $grid = $('.row-news').isotope({
		itemSelector: '.Nieuwsbericht',
		layoutMode: 'fitRows'
	 });
 	  $('.filter.all').click(function() {
			$grid.isotope({ filter: '*' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.algemeen').click(function() {
			$grid.isotope({ filter: '.algemeen' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.1e-selectie').click(function() {
			$grid.isotope({ filter: '.1e-selectie' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.senioren').click(function() {
			$grid.isotope({ filter: '.senioren' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.jeugd').click(function() {
			$grid.isotope({ filter: '.jeugd' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.vrijwilligers').click(function() {
			$grid.isotope({ filter: '.vrijwilligers' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.sponsoren').click(function() {
			$grid.isotope({ filter: '.sponsoren' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.interactief').click(function() {
			$grid.isotope({ filter: '.interactief' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });
	  $('.filter.wedstrijdprogrammas').click(function() {
			$grid.isotope({ filter: '.wedstrijdprogrammas' });
			$('.filter').removeClass('active');
			$(this).addClass('active');
	  });

$grid.on( 'arrangeComplete', function( event, filteredItems ) {
	console.log(event);
	console.log(filteredItems);
});

*/
var days = new Array("Zondag", "Maandag", "Dinsdag",
"Woensdag", "Donderdag", "Vrijdag", "Zaterdag");

var months = new Array("januari", "februari", "maart", 
"april", "mei", "juni", "juli", "augustus", "september", 
"oktober", "november", "december");

function formatDate(date) {
	if (date == null)
		return '';
	return date.substring(8,10) + '-' + date.substring(5,7) + '-' + date.substring(0,4);
}
function formatFancyDate(date) {
	var d = new Date();
	d.setFullYear(date.substring(0,4));
	d.setMonth(date.substring(5,7) - 1);
	d.setDate(date.substring(8,10));
	return days[d.getDay()] + " " + d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear(); 
}
function formatDateShort(date) {
	if (date == null)
		return '';
	return date.substring(8,10) + '-' + date.substring(5,7);
}
function formatTime(time) {
	if (time == null)
		return '';
	return time.substring(0,2) + ':' + time.substring(2,4);
}
function getUrlParameter(param) {
	var url = window.location.search.substring(1);
	var urlVarsPart = url.split('&');
	for (var i = 0; i < urlVarsPart.length; i++) {
		var urlVars = urlVarsPart[i].split('=');
		if (urlVars[0] == param) {
		    return urlVars[1];
		}
	}
};

var knvbMap = {};
knvbMap["ADO"] = "Afgelast door organisatie";
knvbMap["ADB"] = "Afgelast door bond";
knvbMap["ADV"] = "Afgelast door vereniging";
knvbMap["AFG"] = "Afgelast";
knvbMap["BNO"] = "Bezoekend team niet opgekomen";
knvbMap["GOB"] = "Gestaakt wegens onvoldoende spelers uitspeelend team";
knvbMap["GOT"] = "Gestaakt wegens onvoldoende spelers thuisspelend team";
knvbMap["GSB"] = "Geen spelerspassen beide teams";
knvbMap["GSU"] = "Geen spelerspassen uitspelend team";
knvbMap["GVS"] = "Geen vervangende scheidsrechter";
knvbMap["GWO"] = "Gestaakt wegens ongeval";
knvbMap["GWT"] = "Gestaakt wegens tuchtzaak";
knvbMap["GWVU"] = "Gestaakt wegens niet hervatten wedstrijd uitspelend team";
knvbMap["GWW"] = "Gestaakt wegens weersomstandigheden";
knvbMap["NOB"] = "Niet opgekomen beide teams";
knvbMap["SNO"] = "Scheidsrechter niet opgekomen";
knvbMap["TAS"] = "Terreinafkeuring door scheidsrechter";
knvbMap["TNO"] = "Thuisspelend team niet opgekomen";
knvbMap["WNO"] = "Wedstrijdformulier niet ontvangen";
knvbMap["WOV"] = "Wedstrijd ongeldig verklaard";

function knvbErrorText(abbr) {
	var txt = knvbMap[abbr];
	return txt == null ? abbr : txt;
}