$('.my-slider').unslider({
	animation: 'fade', 
	autoplay: true, 
	arrows: true,
	speed: 1500,
	delay: 6000,
	nav:false 
});
	

(function($) {
	$(function() { //on DOM ready 
    		$("#jubileumScroller").simplyScroll();
	});
 })(jQuery);

jQuery(document).ready(function($) {
	$(".InitialContent").on("click", function() {
		var collabisbleSibling = $(this).siblings(".CollapsibleContent");
		var wasClosed = collabisbleSibling.hasClass("Collapsed");
		// first collapse all
		$(".CollapsibleContent").addClass("Collapsed");
		// Then expand the current
		if(wasClosed)
			collabisbleSibling.removeClass("Collapsed");
	});
	
	$(".OpenAanmeldModalButton").on("click", function () {
	     var formData = $(this).siblings(".FormData").html();
	     $(".modal-body #Data").html( formData );
	});
	
	$("#submitModalForm").on("click", function(e) {
		e.preventDefault();
		var myForm = $('FORM.wpcf7-form');
		if(!myForm[0].checkValidity()) {
		  myForm.find('#submitModalFormHidden').click();
		} else {
			$('.Sending').show();
			$('.Success').hide();
			$('#submitModalForm').hide();
		    // $('.modal-body FORM').attr('action', "/webservice/formSubmit").submit();
			$.post(
				'/webservice/formSubmit',
				myForm.serialize(),
				function( data ) {
					$('.Sending').hide();
					$('.Success').show();
					$('.Success').show();
				}
			);
		}
	});
});