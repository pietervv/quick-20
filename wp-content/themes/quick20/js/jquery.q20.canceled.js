jQuery(document).ready(function($){

/***
	schedule
***/
	var getData = function(week) {
		$.ajax({
			url: urlBase + "/webservice/cdCanceled",
			type: "POST",
			data: { "week" : week },
			dataType: "json",
			success: displaySchedule
		});
	}
	
	var displaySchedule = function(data) {
		var canceledRows = "";
		$.each(data["matchDays"], function(matchDay, matches) {
			$("TABLE.SchedulePageTable TR.fallback").remove();
			$.each(matches, function(index, game) {
				var cat = game["displCategory"];
				var linkCat = game["category"] == 'Senioren' ? "senioren" : "jeugd";
				var rowClass = "DataRow " + cat;
				var compType = game['compType'];
				var canceled = game["info"] != null && (game["info"] == "AFG" || game["info"] == "ADO" || game["info"] == "ADB");
				if (canceled) {
					rowClass += " cancelled";
					compType = "Afgelast";
				}
				if (game['goalsHome'] != null && game['goalsAway'] != null) {
					compType = game['goalsHome'] + " - " + game['goalsAway'];
				}
				row = "<tr class='Clear " + rowClass + "'";
				row += "><td>" + matchDay + "</td>";
				row += "<td>" + game["time"] + "</td>";
				if (game["quickHome"])
					row += "<td class='Quick'><a href='/teams/" + linkCat + "?id=" + game["teamId"] + "&ref=programma'>" + game["home"] + "</a></td>";
				else
					row += "<td>" + game["home"] + "</td>";
					if (!game["quickHome"])
					row += "<td class='Quick'><a href='/teams/" + linkCat + "?id=" + game["teamId"] + "&ref=programma'>" + game["away"] + "</a></td>";
				else
					row += "<td>" + game["away"] + "</td>";
				row += "<td class='info' title='" + game['compType'] + "'>" + compType + "</td>";
				row += "</tr>";
				canceledRows += row;
			});
		});
		if (canceledRows != "") {
			$("TABLE.CanceledTable").append(canceledRows);
			$("TABLE.CanceledTable TD.info").remove();
			$("DIV.CanceledContainer").show();
			$("TABLE.CanceledTable").show();
		} else {
			$("TABLE.CanceledTable .HeaderRow").hide();
			$("TABLE.CanceledTable").append("<tr><td colspan='4'>Er zijn geen afgelastingen deze week.</td></tr>");
		}
		$(".LoadingIcon").hide();
	};

	$("DIV.ProgrammaPage").each(function() {
		getData(0);	
	});
	
	$(".FilterSelection").click(function() {
		$(this).toggleClass("active");
		var clickedLI = $(this).parent();
		clickedLI.addClass("active");
		clickedLI.siblings("LI").removeClass("active");
		var text = $(this).text();
		$(this).parents("LI.dropdown").find(".FilterTitle").text(text);
		displayRows();
    });
    
    $("SELECT.WeekSelect").on("change", function() {
    	resetPage();
    	$(".LoadingIcon").show();
    	var selecedWeek = $(this).val();
    	getData(selecedWeek);
    });
});

function getValues(selector, listOfAllValues) {
	var selected = $("UL." + selector + " LI.active").attr("value");
	var valueMap = [];
	if (selected == "All")
	return listOfAllValues;
		valueMap.push(selected);
	return valueMap;
}

function displayRows(tableClass) {
	$("TABLE.SchedulePageTable TR").not(".HeaderRow").hide();
	var groups = getValues("group", ["senior", "youth"]);
	var times = getValues("time", ["Weekend", "Week"]);
	var places = getValues("place", ["Home", "Away"]);
	for (var g = 0; g < groups.length; g++) {
		var group = groups[g];
		for (var t = 0; t < times.length; t++) {
			var time = times[t];
			for (var p = 0; p < places.length; p++) {
				var place = places[p];
				var selector = group + time + place;
				$("TABLE.SchedulePageTable TR." + selector).show();
			}
		}
	} 
}

var resetPage = function() {
	$(".Clear").remove();
};