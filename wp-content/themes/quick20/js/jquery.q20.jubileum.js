jQuery(document).ready(function($) {
	$("DIV.done").hide();
	
	$("FORM").submit(function( event ) {
		$.ajax({  
			type: "POST",  
			url: $(this).attr('action'),
			data: $(this).serialize(),
			complete: function() {
				$("DIV.done").show();
				$('html, body').animate({scrollTop: $("BODY").offset().top}, 500);
			}  
		});
		event.preventDefault();
	});
});