<?php
/*
Template Name: LiveBlog

*/
?>
<?php get_header(); ?>
	<div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
		    </div>
	    </div>
	</div>
    
	<div class="LiveBlog">
		<div class="container">
			<div id="24lb_thread" class="lb_1330827"></div>
			<script type="text/javascript">
			(function() {
			var lb24 = document.createElement('script'); lb24.type = 'text/javascript'; lb24.id = '24lbScript'; lb24.async = true; lb24.charset="utf-8";
			lb24.src = '//v.24liveblog.com/embed/24.js?id=1330827';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(lb24);})();
			</script>
		</div>
	</div> 
	<?php include 'footer.php';?>
