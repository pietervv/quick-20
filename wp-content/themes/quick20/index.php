<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
		    </div>
	    </div>
    </div>
    
    <div class="AtmospherePics">
		<div class="col-md-4 AtmospherePicture <?php the_field('uitlijning_sfeerfoto1'); ?>" style="background-image: url(<?php the_field('sfeer_foto_1'); ?>);"></div>
		<div class="col-md-4 AtmospherePicture <?php the_field('uitlijning_sfeerfoto2'); ?>" style="background-image: url(<?php the_field('sfeer_foto_2'); ?>);"></div>
		<div class="col-md-4 AtmospherePicture <?php the_field('uitlijning_sfeerfoto3'); ?>" style="background-image: url(<?php the_field('sfeer_foto_3'); ?>);"></div>
	</div>    
	
<?php
if( have_rows('content') ):
while ( have_rows('content') ) : the_row(); ?>
	<div class="FullWidth">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<?php the_sub_field('Kolom'); ?>
			</div>
		</div>
	</div>
<?php
endwhile;
else :
endif;
?>

		<?php include 'footer.php';?>