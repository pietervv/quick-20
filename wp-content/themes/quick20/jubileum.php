<?php
/*
Template Name: Jubileum-ideeenbus
*/
?>

<?php get_header(); ?>

<div class="MainTitle">
    <div class="container ContainerMainTitle">
	    <div class="col-xs-12">
	    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
	    </div>
    </div>
</div>
<div class="container">
	<div class="AnniversaryPage">
		<div class="rowz done">
			Bedankt voor het doorgeven van je idee!
		</div> 
		<form action="/webservice/sendmail" method="post">
			<div class="rowz content">
				In 2020 bestaat Quick'20 honderd jaar! In dat jaar zullen diverse activiteiten en 
				festiviteiten plaatsvinden om dit met zijn allen te vieren. Een jubileumcommissie, 
				onder leiding van Engelbert Heideman, is al gestart met het maken van plannen. 
				We zijn heel erg benieuwd naar alle ideeën die leven binnen de vereniging over het 
				jubileum, dus deel ze met ons. Hoe meer input, hoe beter!
			</div>
			<div class="rowz name">
				<div class="lbl">*Naam:</div>
				<div class="field"><input type="text" name="sender" class="txt"/>	</div>			
			</div>
			<div class="rowz email">
				<div class="lbl">*Email:</div>
				<div class="field"><input type="text" name="email" class="txt"/></div>
			</div>
			<div class="rowz comment">
				<div class="lbl">Idee:</div>
				<div class="field"><textarea name="comment"></textarea></div>
			</div>
			<div class="rowz remark">
				* niet verplicht om in te vullen
			</div>
			<div class="rowz submit">
				<input type="submit" value="Verstuur" />
			</div>
		</form>
	</div>
</div>
<?php include 'footer.php';?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileum.js"></script>