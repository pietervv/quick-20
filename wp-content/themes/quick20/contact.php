<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Contact</h1>
		    </div>
	    </div>
    </div>
    
	<div class="ContactHeader">
		<div class="col-sm-6 ContactHeaderPic" style="background-image: url(<?php the_field('afbeelding_1'); ?>);"></div>
		<div class="col-sm-6 ContactHeaderPic" style="background-image: url(<?php the_field('afbeelding_2'); ?>);"></div>
	</div>
	
	<div class="ContactContent">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-xs-12 col-left col-right">
				<div class="col-sm-4 col-xs-12 Adres">
					<div class="col-xs-12 col-left"><h2>Adres</h2></div>
					<div class="col-xs-12 col-left AdresData">
						<p>
							<?php the_field('adresgegevens'); ?>
						</p>
					</div>
				</div>
				<div class="col-sm-8 col-xs-12 Maps col-right">
					<div class="col-xs-12"><h2>Maps</h2></div>
					<div class="col-xs-12"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1219.5986539043572!2d6.9172529239711436!3d52.31242209999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47b8174bfa5557cd%3A0x22c81858a7e7ed17!2sK.V.V.+Quick+&#39;20!5e0!3m2!1snl!2snl!4v1451993583114" width="100%" height="268" frameborder="0" style="border:0" allowfullscreen></iframe></div>
				</div>
			</div>
			
			<div class="col-lg-offset-1 col-lg-10 col-sm-12 col-left col-right">
				<div class="col-sm-4 col-xs-12 Adres">
					<div class="col-xs-12 col-left"><h2 class="black">Contact</h2></div>
					<div class="col-xs-12 col-left AdresData">
						<p>
							<?php the_field('contactgegevens'); ?>
						</p>
					</div>
				</div>
				<div class="col-sm-8 OverigContact">
					<div class="col-sm-6 Adres">
						<div class="col-xs-12 col-left"><h2 class="black">Overig Contact</h2></div>
						<div class="col-xs-12 col-left AdresData">

								<?php the_field('e-mailadressen'); ?>

						</div>
					</div>
					<div class="col-sm-6 Adres col-right">
						<div class="col-xs-12 col-left"><h2 class="black ContactFormTitle">Contactformulier</h2></div>
						<div class="col-xs-12 col-left">
							
							<?php echo do_shortcode('[contact-form-7 id="13466" title="Contactformulier 1"]'); ?>
							
							<!-- Contactformulier via WordPress -->
							
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>	
	
<?php include 'footer.php';?>