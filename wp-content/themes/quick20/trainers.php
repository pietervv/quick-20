<?php
/*
Template Name Posts: Trainers
Template Name: Trainers

*/
?>
<?php get_header(); ?>
	<div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
		    </div>
	    </div>
	</div>
    
	<div class="TrainersPage">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<div class="row">
					<div class="col-md-3 col-xs-12 Sidebar">
						<ul class="ContentSwitch">
							<li class="SwitchItem active" value="Overview">Algemeen</li>
							<!-- li class="SwitchItem" value="Policy">Technisch beleid</li-->
							<li class="SwitchItem" value="Pupils">Pupillen</li>
							<li class="SwitchItem" value="Juniors">Junioren</li>
							<li class="SwitchItem" value="Lifestyle">Lifestyle</li>
							<li class="SwitchItem" value="Experiences">Praktijkervaringen</li>
							<!--li class="SwitchItem" value="WhoIsWho">Smoelenboek</li-->
						</ul>
					</div>
					<div class="col-md-9 col-xs-12 ContentBlock">
						<div class="Content Overview">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Algemene informatie</h2>
								</div>
							</div>
							<div class="col-lg-offset-1 col-lg-10 col-xs-12">
								<div class="row">
									<?php the_field('inleiding'); ?>
								</div>
							</div>
							<div class="col-lg-offset-1 col-lg-10 col-xs-12">
								<div class="col-xs-12 Video">
									<?php the_field('video'); ?>
								</div>
							</div>
						</div> <!--/Overview-->
						<!--div class="Content Policy">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Technisch beleid</h2>
								</div>
							</div>
						</div--> <!--/Policy-->
						<div class="Content Pupils">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Pupillen</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-10 col-xs-12">
									<p>
									De oefenstof die op deze pagina wordt aangeboden past in het technisch beleidsplan en draagt bij aan ontwikkeling en 
									professionalisering van alle trainingen. Waar mogelijk zijn per oefening de specifieke leeftijdsgroepen beschreven.									</p>
									</p>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Frequentie-oefening<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• Geen</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• Spelers maken met beide benen pasjes voorwaarts en achterwaarts op de zijlijn (40 seconden lang)</br>
											• Spelers springen zijwaarts over de zijlijn (40 seconden lang)</br>
										<b>Doel van de oefening:</b></br>
											• Trainen frequentie</br>
											• Trainen ritme</br>
										</br>
										<div class="ExercisesVideo Init" data-id="Mk4YrddPe14"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Sprintoefening<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 5 petjes</br>
										<b>Aantal personen:</b></br>
											• 9 spelers</br>
										<b>Oefening:</b></br>
											• Sprintoefening waarbij spelers kort en fel om de as moeten draaien tussen 2 petjes om vervolgens een sprint af te leggen van 10 meter (3 keer per speler)</br>
											• Sprintoefening waarbij spelers kort en fel om de as draaien tussen 2 petjes om vervolgens een sprint af te leggen van 30 meter (3 keer per speler)</br>
										</br>
										<div class="ExercisesVideo" data-id="a5JL9YYeChU"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm, middelste speler werkt<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 12 petjes</br>
											• 12 ballen</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• 6 groepjes van 3 spelers waarbij de middelste speler werkt
(kaatsen bal naar spelers aan de zijkant die de bal inspelen, passen bal die door de lucht wordt aangegooid, koppen bal die door de lucht wordt aangegooid,
bal aannemen op borst en vervolgens terugkaatsen, uitdraaien (met 1 bal)</br>
										<b>Doel van de oefening:</b></br>
											• Trainen kaatsen</br>
											• Trainen passen</br>
											• Trainen nauwkeurigheid</br>
											• Trainen balbeheersing</br>
										</br>
										<div class="ExercisesVideo Init" data-id="PmtRLbc7tY0"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm van pion naar pion<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 16 petjes</br>
											• 9 ballen</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• Oefening waarbij spelers dribbelen naar pion, daar kapbeweging (binnenkant, buitenkant, achter het standbeen of overstap maken) en bal terugspelen (strakke bal spelen)</br>
										<b>Doel van de oefening:</b></br>
											• Trainen van dribbelen</br>
											• Trainen passing</br>
											• Trainen kapbeweging</br>
											• Trainen handelingssnelheid</br>
											• Trainen balbeheersing</br>
										</br>
										<div class="ExercisesVideo Init" data-id="gsP-EUyPeu8"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm 1 tegen 1 / 1 tegen 2<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 2 kleine doeltjes</br>
											• 6 petjes</br>
											• 4 ballen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Bal wordt schuin van achter het doel door speler ingespeeld naar tegenstander. Er wordt nu een 1 tegen 1 duel uitgespeeld. Indien de aanvallende speler met de bal in het midden is gepasseerd, mag er op beide doelen worden gescoord.</br>
											• Breng spelelement in door punten te tellen</br>
											• Oefening kan uitgebreid worden naar 1 tegen 2 en 2 tegen 3</br>
											• Er kan ook gespeeld worden met de doeltjes</br>
										<b>Doel van de oefening:</b></br>
											• Eén worden met de bal in de kleine ruimte</br>
											• Trainen van strakke aanvalspass</br>
											• Trainen bal aanname aanvaller</br>
											• Ritme houden bij aanvaller</br>
											• Handelingssnelheid verhogen</br>
										</br>
										<div class="ExercisesVideo Init" data-id="EzLEtjIwG-c"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Afwerkvormen<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• Ladder</br>
											• 15 petjes</br>
											• 9 ballen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers, waarvan 1 keaper (1 team)</br>
										<b>Oefening:</b></br>
											• Speler met bal speelt naar andere speler en deze kaatst de bal terug. Inspelende speler rondt af (let op
schuine lijnen, niet recht inspelen). Zowel van links als van rechts oefening doen.</br>
											• Speler met bal speelt andere speler in, deze speler draait uit en rondt af.
Trainer staat achter uitdraaiende speler. Oefening weer in schuine lijn. Let op strakke inspeelpass en uitdraaien dat beide benen worden gebruikt.</br>
											• Speler maakt actie op achterlijn en speelt inkomende speler (vanaf 16 meter) in, welke afrondt. Let op inspeelpass en bal aanname.
Zowel vanaf links als rechts oefenen.</br>
											• Speler dribbelt door harmonica, speelt de trainer of speler aan en rondt af</br>
											• Speler loopt met hoge frequentie door ladder en rondt de bal af, welke door de trainer is klaargelegd. Let op het houden van ritme nadat de ladder oefening is gedaan. Houd snelheid bij afronden. </br>
											• Speler dribbelt naar trainer toe en wipt de bal in de handen van de trainer. Deze gooit de bal in de lucht, speler rondt af middels dropkick.</br>
											• Speler speelt de bal in naar andere speler, die de bal verlegt naar speler die staat op de rechter- of linkerkant (de speler die het spel verlegt en
de spelers die aan de zijkanten staan, blijven even staan). De speler die de bal heeft ingespeeld moet een loopactie maken om de speler heen die de bal verlegd heeft, om vervolgens af te ronden (2 petjes gebruiken).</br>
										<b>Doel van de oefening:</b></br>
											• Een worden met de bal</br>
											• Trainen strakke inspeelpass</br>
											• Trainen bal aanname</br>
											• Trainen afwerken op doel (lichaam voorover en snelheid houden)</br>
										</br>
										<div class="ExercisesVideo Init" data-id="IXH1l-JvLCg"></div>
										<div class="ExercisesVideo Init" data-id="xAxQUcfH44I"></div>
										<div class="ExercisesVideo Init" data-id="3OgIFgdP_II"></div>
										<div class="ExercisesVideo Init" data-id="clX66syjSCM"></div>
										<div class="ExercisesVideo Init" data-id="ROQDuAbMnLM"></div>
										<div class="ExercisesVideo Init" data-id="1fKrHA7ZPx0"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Positiespellen<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 4 petjes</br>
											• 2 ballen</br>
											• 3 hesjes in verschillende kleuren</br>
										<b>Aantal personen:</b></br>
											• 8 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Spelvorm 6 tegen 2: 2 personen staan in het midden, deze moeten de bal zien te veroveren van de personen die aan de zijkanten staan. Spelelement mag aangebracht worden.</br>
											• Spelvorm 6 tegen 2 met kaatser: 2 personen staan in het midden, deze moeten de bal zien te veroveren van de personen die aan de zijkanten staan. 1 van hen fungeert als kaatser in het midden.
											• Spelvorm 2 tegen 2 met 4 vaste personen aan de zijkanten: de personen die in het midden staan mogen de personen aan de zijkanten gebruiken bij balbezit. De spelers aan de zijkanten worden een team met de in balbezit zijnde partij.
											• Spelvorm 3 tegen 3 met 2 vaste spelers aan de zijkanten, deze vormen een team met het in balbezit zijnde team.
										<b>Doel van de oefening:</b></br>
											• Balcontrole houden in kleine ruimte
											• Speler moet in de open stand staan</br>
											• Trainen van driehoekjes door kaatser te gebruiken</br>
											• Trainen van balbezit hebben en bal in de ploeg houden</br>
											• Trainen van de ruimte leren te gebruiken. Bij balbezit veld groot maken. Ruimte ligt aan de zijkanten</br>
											• Trainen bal aannames en passing.</br>
										</br>
										<div class="ExercisesVideo Init" data-id="mRysnM1yqgs"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Warming up<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 6 petjes</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• Te gebruiken voor elke wedstrijd en training om in beweging te komen</br>
											• Oefenvorm kennen de spelers</br>
											• Let op kleine dribbelpassjes bij start oefening.</br>
										</br>
										<div class="ExercisesVideo Init" data-id="blCP_tsJpUI"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm grote rechthoek<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 4 petjes</br>
											• 18 ballen</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• Spelers dribbelen door rechthoek en krijgen daarbij instructie over uit te voeren actie (snelheid van dribbelen opvoeren, de bal gaan stoppen en op de bal gaan zitten, kapbeweging binnenkant voet, kapbeweging buitenkant voet, kapbeweging achter het standbeen, schaarbeweging, zidane, akka, dansen op de bal etc.)</br>
											• 13 spelers verdelen zich over de zijkanten en 5 spelers blijven met bal in het midden staan. Pass en aanname oefening. De spelers in het midden passen de bal naar speler die van buitenkant in komt lopen</br>
											• Hooghouden in vierkant en bal opgooien en meenemen.</br>
										<b>Doel van de oefening:</b></br>
											• Eén worden met de bal (balbeheersing)</br>
											• Vergroten handelingssnelheid</br>
											• Rust bewaren in drukke, kleine ruimte (balcontrole)</br>
											• Oefenen acties</br>
											• Oefening passing in kleine ruimte; moet strak zijn</br>
											• Bal aanname in kleine ruimte; moet snel zijn</br>
										</br>
										<div class="ExercisesVideo Init" data-id="fXCXgS6MLHc"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm: kruis<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 8 petjes</br>
											• 8 ballen</br>
										<b>Aantal personen:</b></br>
											• 8 spelers (1 team, alle veldspelers)</br>
										<b>Oefening:</b></br>
											• 4 spelers dribbelen tegelijk naar vierkant en slaan linksaf of rechtsaf. Spelers komen in het midden andere dribbelende spelers tegen. In het midden wordt een kapbeweging, bal achter standbeen, zidane gemaakt.</br>
											• 4 spelers dribbelen tegelijkertijd rechtdoor door kruis, komen andere spelers onderweg tegen.</br>
										<b>Doel van de oefening:</b></br>
											• Balbeheersing</br>
											• Beide benen leren gebruiken</br>
											• Over de bal leren kijken</br>
										</br>
										<div class="ExercisesVideo Init" data-id="o8Tk_mFE4fw"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm: harmonica<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 16 petjes</br>
											• 9 ballen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Spelers dribbelen door harmonica en moeten bij elk petje een door de trainer aangegeven actie uitvoeren (kapbeweging, kapbeweging achter het standbeen, zidane, etc)</br>
										<b>Doel van de oefening:</b></br>
											• Balbeheersing</br>
											• Beide benen leren gebruiken</br>
											• Over de bal heenkijken</br>
											• Oefening eerst trainen op beheersing, vervolgens oefening uitbreiden op snelheid</br>
										</br>
										<div class="ExercisesVideo Init" data-id="lxVNTn2bdUM"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm: passen en kaatsen<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 3 petjes</br>
											• 3 ballen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Oefening waarbij er getraind wordt op korte pass, lange pass, kaats, balaanname en dribbelen. Speler speelt bal in naar andere speler, die voor de kaats komt. De inspelende speler speelt een lange bal naar weer een andere speler, die middels een snelle aanname verder dribbelt</br>
										<b>Doel van de oefening:</b></br>
											• Korte strakke pass oefenen</br>
											• Lange strakke pass oefenen</br>
											• Kaats oefenen</br>
											• Dribbelen oefenen</br>
											• Trainen van zuiverheid</br>
										</br>
										<div class="ExercisesVideo Init" data-id="Ip4IFXDC7kg"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Techniekvorm: dribbelen met kleuren (tevens conditievorm)<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 16 petjes (4 verschillende kleuren)</br>
											• 9 ballen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Oefening waarbij spelers dribbelen van kleur naar kleur en de bal stoppen op commando van de trailer, of op de bal gaan zitten. Spelelement toevoegen.</br>
										<b>Doel van de oefening:</b></br>
											• Handelingssnelheid vergroten</br> 
											• Balbeheersing</br>
										</br>
										<div class="ExercisesVideo Init" data-id="b8MsgcX9fIw"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Coördinatieoefeningen<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• 24 petjes</br>
										<b>Aantal personen:</b></br>
											• 18 spelers (2 teams)</br>
										<b>Oefening:</b></br>
											• Oefening waarbij de coördinatie getraind wordt in combinatie met sprint. Speler hinkelt op linkerbeen naar petje en zet hinkelen bij de pion om in sprinten.</br>
										<b>Doel van de oefening:</b></br>
											• Trainen van beheersing lichaam</br> 
											• Vanuit oefening sprinten trainen (explosie)</br>
											• Trainen stabiliteit</br>
										</br>
										<div class="ExercisesVideo Init" data-id="tDCODAXVNaM"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-offset-1">
								<div class="CollapsableGroup Large" collapsed="1">
									<div class="CollapsableTitle">Ladderoefeningen<span class="down"></span></div>
									<div class="CollapsableContent">
										<b>Benodigde materialen:</b></br>
											• Loopladder</br>
											• 3 ballen</br>
											• 6 pionnen</br>
										<b>Aantal personen:</b></br>
											• 9 spelers (1 team)</br>
										<b>Oefening:</b></br>
											• Door ladder lopen met knieën hoog, met hoog ritme, zijwaarts met knieën hoog, zijwaarts met hoog ritme, uitstappen links en rechts indien speler door ladder aanzetten voor sprintje door pionnen. Sprinten zijwaarts door pionnen</br>
											• In de ladder bewegen met hoog tempo, op commando van de trainer uitstappen om vervolgens de bal te kaatsen met andere speler die de bal aanspeelt</br>
										<b>Doel van de oefening:</b></br>
											• Trainen ritme kleine pasjes</br>
											• Trainen frequentie</br>
											• Trainen uithoudingsvermogen</br>
											• Trainen lichaamsbeheersing</br>
										</br>
										<div class="ExercisesVideo Init" data-id="XFkF1M38akY"></div>
									</div>
								</div>
							</div>
						</div> <!--Pupils-->

						<div class="Content Juniors">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Junioren</h2>
								</div>
								<div class="col-lg-10 col-xs-12">
									<div class="row">
										<p>Op deze pagina vind je trainingsvormen voor junioren. Kies hieronder een categorie:</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="filters">
										<div class="collapse navbar-collapse filter-menu" id="bs-example-navbar-collapse-2">
											<ul class="nav navbar-nav">
												<li class="dropdown">
													<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
														<span class="FilterTitle"></span>
													</a>
													<ul class="dropdown-menu group">
														<?php
															if (have_rows("trainingsvormen_junioren")):
																$idx = 0;
																while ( have_rows("trainingsvormen_junioren") ) : the_row();
																	$catName = get_sub_field("naam");
																	?>
																		<li class="filter <?php if($idx == 0): echo "active"; endif; ?>" value="<?php echo $catName ?>"><a class="FilterSelection"><?php echo $catName ?></a></li>
																	<?php
																	$idx++;	
																endwhile;
															endif;
														?>
										        	</ul>
										        </li>
									        </ul>
							  			</div>
						  			</div>
								</div>
							</div>
							<div class="Excersises Pdf">
								<?php
									if (have_rows("trainingsvormen_junioren")):
										while ( have_rows("trainingsvormen_junioren") ) : the_row();
											$catName = get_sub_field("naam");
											?><div class="OefenvormGroep" value="<?php echo $catName ?>"><?php
											while ( have_rows("oefenvormen") ) : the_row();
												$file = get_sub_field("pdf");
												?>
													<div class="Oefenvorm">
														<a href="<?php echo $file['url']; ?>" target="_blank">
															<img src="<?php bloginfo('template_directory'); ?>/images/pdf_icon.png">
															<?php the_sub_field("titel"); ?>
														</a>
													</div>
												<?php
											endwhile;
											?></div><?php
											$idx++;	
										endwhile;
									endif;
								?>
							</div>
						</div> <!--Juniors-->
						<div class="Content Lifestyle">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Lifestyle</h2>
									<h1>Waar op te letten bij het kopen van voetbalschoenen?</h1>
									<p>
									Tussen de verscheidende voetbalschoenen die er te verkrijgen zijn op de markt zitten veel verschillen. 
									Bij het kopen van nieuwe voetbalschoenen is het goed om te letten op de volgende punten.
									<h4>Pasvorm van de schoen / schoenmaat / vetersluiting</h4>
									Zorg ervoor dat je schoenen op de juiste maat en pasvorm koopt. Koop geen te kleine/grote of te smalle/brede schoenen.
									</p>
									<h4>Stabiliteit loopzool</h4>
									<p>
									Let goed op de stabiliteit van de loopzool. Het buigpunt van de loopzool moet onder de voorvoet zitten:
									</p>
									<img src="<?php bloginfo('template_directory'); ?>/images/trainer/buigpunt.jpg"></img>
									<h4>Pasvorm hielkap (contrefort)</h4>
									<p>
									De vorm van de hielkap moet overeenkomen met de vorm van je hiel. 
									Bij een ronde hielvorm kun je het best een schoen kiezen die ook 
									een ronde vorm van de hielkap heeft (zie de afbeelding hieronder).
									</p>
									<img src="<?php bloginfo('template_directory'); ?>/images/trainer/hielkap.jpg"></img>
									<h4>Noppen</h4>
									<p>
									Houdt goed rekening welke noppen je kiest rekening houdend met de 
									ondergrond waar je op speelt. Het voorbeeld hieronder is een schoen 
									die goed geschikt is voor kunstgras en normale grasvelden (onder normale omstandigheden).
									</p>
									<img src="<?php bloginfo('template_directory'); ?>/images/trainer/noppen.jpg"></img> 
								</div>
							</div>
						</div> <!--Lifestyle-->
						<div class="Content Experiences">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Praktijkervaringen</h2>
								</div>
							</div>
						</div> <!--Experiences-->
						<!-- div class="Content WhoIsWho">
							<div class="row">
								<div class="col-lg-offset-1 col-md-10 col-xs-12">
									<h2 class="black">Smoelenboek</h2>
								</div>
							</div>
						</div --> <!--WhoIsWho-->
					</div>
				</div>
			</div>
		</div>
	</div> 
	<?php include 'footer.php';?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.trainers.js"></script>