jQuery(document).ready(function($) {
	// Clubinfo
	$("A.LoadLink").on("click", function(event) {
		event.preventDefault();
		$(".LoadingInProgress").show();
		$(".ClubInfo").hide();
		$.ajax({  
			type: "GET",  
			url: $(this).attr('href'),
			success: function(data) {
				if (!data) {
					$(".Message").html("Laden mislukt!");
					$(".Message").removeClass("Success");
					$(".Message").addClass("Error");
				} else {
					$(".Message").html("Laden gelukt! Aantal nieuwe clubs: " + data + ".");
					$(".Message").addClass("Success");
					$(".Message").removeClass("Error");
				}
				$(".LoadingInProgress").hide();
				$(".ClubInfo").show();
			}  
		});
	});
	
	$(".ClubInfo FORM").submit(function( event ) {
		$.ajax({  
			type: "POST",  
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data) {
				if (!data) {
					$(".Message").html("Opslaan mislukt!");
					$(".Message").removeClass("Success");
					$(".Message").addClass("Error");
				} else {
					$(".Message").html("Opslaan gelukt! Aantal wijzigingen: " + data + ".");
					$(".Message").addClass("Success");
					$(".Message").removeClass("Error");
				}
			}  
		});
		event.preventDefault();
	});
	
	// Einde Clubinfo
	$(".team_select").change(function() {
		var teamId = $(this).val();
		$(".form_team_id").val(teamId);
		$.ajax({
			url: "/webservice/teamDetails",
			type: "POST",
			data: { "teamId" : teamId },
			dataType: "json",
			success:  function(data) {
				var imageLocation = data["picture"];
				var imgStr = imageLocation != null && imageLocation != '' ? "<img src='/wp-content/uploads/TeamPictures/" + imageLocation + "'></img>" : "";
				$("#current_pic").html(imgStr);
				$("#current_name").html(data["team"]["name"]);
				$("#submitbutton").attr("value", "Upload teamfoto voor " + data["team"]["name"]);
				if(imgStr != "")
					$(".delete_current").show();
				else
					$(".delete_current").hide();
				$("DIV#loading").hide();
			}
		});
	});
	$(".team_select").trigger("change");
	$('FORM#upload_teamfoto').each(function() {
		$(this).submit( function(e) {
			$("DIV#loading").show();
			$.ajax( {
				url: '/webservice/imageupload/',
				type: 'POST',
				data: new FormData(this),
				processData: false,
				contentType: false,
				success: function(data) {
					location.reload();
				}
			});
			e.preventDefault();
		});
	});
	
	$('FORM#delete_teamfoto').each(function() {
		$(this).submit( function(e) {
			$("DIV#loading").show();
			$.ajax( {
				url: '/webservice/deleteimage/',
				type: 'POST',
				data: new FormData(this),
				processData: false,
				contentType: false,
				success: function(data) {
					location.reload();
				}
			});
			e.preventDefault();
		});
	});
	
	$(".uitleg .titel").on("click", function() {
		$(".uitleg .tekst").slideToggle();
	});
	
	$(".JubileumTable .delete").on("click", function() {
		var id = $(this).attr("db_id");
		$.ajax({  
			type: "POST",  
			url: "/webservice/deleteComment",
			data: { "id" : id },
			dataType: "json",
			complete: function() {
				location.reload();
			}  
		});
	});
});