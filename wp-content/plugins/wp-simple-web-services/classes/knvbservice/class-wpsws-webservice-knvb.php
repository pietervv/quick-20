<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_knvb {
	private static $instance = null;

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_competition', array( $this, 'competition' ) );
		add_action( 'wpsws_webservice_knvbBanner', array( $this, 'knvbBanner' ) );
		add_action( 'wpsws_webservice_allCompetitionResults', array( $this, 'allCompetitionResults' ) );
		add_action( 'wpsws_webservice_allCompetitionSchedule', array( $this, 'allCompetitionSchedule' ) );
		add_action( 'wpsws_webservice_oldmatchDetails', array( $this, 'oldmatchDetails' ) );
		add_action( 'wpsws_webservice_schedule', array( $this, 'schedule' ) );
		add_action( 'wpsws_webservice_urltest', array( $this, 'urltest' ) );
		add_action( 'wpsws_webservice_teamSummary', array( $this, 'teamSummary' ) );
	}
	
	public function teamSummary() {
		$teamId = $_POST['teamId'];
		$result = WSMacro::retreiveKnvbData("/teams/" . $teamId . "/results", null);
		$nextMatch = WSMacro::retreiveKnvbData("/teams/" . $teamId . "/schedule", null);
		if ($nextMatch["errorcode"] != 1000) {
			$currWeek = date("W");
			for ($n = 0; $n < 20; $n++) {
				$currWeek = $currWeek + 1;
    			$params = array('weeknummer' => $currWeek);
    			$nextWeek = WSMacro::retreiveKnvbData("/teams/" . $teamId . "/schedule", $params);
    			if ($nextWeek["errorcode"] == 1000) {
    				$nextMatch = $nextWeek;
    				break;
    			}
			}
		}
		$ranking = WSMacro::retreiveKnvbData("/teams/" . $teamId . "/ranking", null);
		$nextMatch = null;
		$ranking = null;
		$result = null;
		WPSWS_Output::get()->output(array('nextMatch' => $nextMatch, 'ranking' => $ranking, 'result' => $result));
	}

	public function knvbBanner() {
		$baseUrl = 'http://api.knvbdataservice.nl/banner.php';
		$bannerData = WSMacro::retreiveKnvbData("/banners", null)["List"];
		$token = $bannerData["token"];
		$clubCode = $bannerData["clubid"];
		$bannerurl = $baseUrl . '?token=' . $token . '&clubid=' . $clubCode . '&banner=rectangle';
		WPSWS_Output::get()->output($bannerurl);
	}

	public function oldmatchDetails() {
		$matchId = $_POST['matchId'];
		$matchDetails = WSMacro::retreiveKnvbData("/wedstrijd/" . $matchId, null);
		WPSWS_Output::get()->output(array('matchDetails' => $matchDetails));
	}

	public function competition() {
		$knvbId = $_POST['teamId'];
		$params_all = array('weeknummer' => 'A');
		$competitieCall = WSMacro::retreiveKnvbData("/competities/" . $knvbId, null);
		$overview = $competitieCall["List"];
		$competitions = array();
		foreach ($overview as $competition) {
			$compType = $competition['CompType'];
			$pouleId = $competition["PouleId"];
			$path = "/competities/" . $knvbId . "/" . $competition["District"] . "/" . $competition["CompId"] . "/" . $competition["ClassId"] . "/" . $pouleId;
			$params = null;
			if ($compType == 'B' || $compType == 'N')
				$params = array('weeknummer' => 'A');
			$compResults = WSMacro::retreiveKnvbData($path  . "/results", $params);
			$compRanking = WSMacro::retreiveKnvbData($path  . "/ranking", array('periode' => 'A'));
			$ranking = array();
			foreach($compRanking["List"] as $team) {
				$period = $team["Periode"];
				$periodRanking = $ranking[$period];
				if ($periodRanking == null)
					$periodRanking = array();
				$info = WSMacro::getClubInfo($team["ClubNummer"]);
				$team["AddedInfo"] = $info;
				$periodRanking[] = $team;
				$ranking[$period] = $periodRanking;
			}
			$hasPeriods = count($ranking) > 1;
			// Normale competitie: komende 3 wedstrijden ophalen voor tab Programma
			if ($compType == 'R') {
				$compSchedule = array();
				$found = 0;
				$currWeek = date("W");
				for ($n = 0; $n < 12; $n++) {
					$params = array('weeknummer' => $currWeek);
	    			$nextWeek = WSMacro::retreiveKnvbData($path  . "/schedule", $params);
	    			if ($nextWeek["errorcode"] == 1000) {
	    				$compSchedule[intval($currWeek)] = $nextWeek;
	    				$found++;
	    			}
	    			if ($found >= 3)
	    				break;
	    			$currWeek++;
				}
			} else {
				$compSchedule = WSMacro::retreiveKnvbData($path  . "/schedule", $params);
			}
			$compDetails = array();
			$compDetails['type'] = $compType;
			$compDetails['id'] = $pouleId; // for sorting
			$compDetails['overview'] = $competition;
			$compDetails['results'] = $compResults;
			$compDetails['ranking'] = $ranking;
			$compDetails['schedule'] = $compSchedule;
			$compDetails['hasPeriods'] = $hasPeriods;
			$competitions[] = $compDetails;
		}
		WPSWS_Output::get()->output(array('competitions' => $competitions));
	}

	public function allCompetitionResults() {
		$knvbId = $_POST['teamId'];
		$competitieCall = WSMacro::retreiveKnvbData("/competities/" . $knvbId, array('comptype' => 'R'));
		$overview = $competitieCall["List"][0];
		$path = "/competities/" . $knvbId . "/" . $overview["District"] . "/" . $overview["CompId"] . "/" . $overview["ClassId"] . "/" . $overview["PouleId"] . '/results';
		$allResults = WSMacro::retreiveKnvbData($path, array('weeknummer' => 'A'));
		if ($allResults["errorcode"] == 1000) {
			$matchDays = array();
			foreach ($allResults["List"] as $result) {
				$matchDay = $result["WedstrijdDag"];
				$matchDayArray = $matchDays[$matchDay];
				if ($matchDayArray == null)
					$matchDayArray = array();
				$matchDayArray[] = $result;
				$matchDays[$matchDay] = $matchDayArray;
			}
			WPSWS_Output::get()->output(array('matchDays' => $matchDays));
		} else
			WPSWS_Output::get()->output(array('allResults' => $allResults["errorcode"]));
	}

	public function allCompetitionSchedule() {
		$knvbId = $_POST['teamId'];
		$competitieCall = WSMacro::retreiveKnvbData("/competities/" . $knvbId, array('comptype' => 'R'));
		$overview = $competitieCall["List"][0];
		$path = "/competities/" . $knvbId . "/" . $overview["District"] . "/" . $overview["CompId"] . "/" . $overview["ClassId"] . "/" . $overview["PouleId"] . '/schedule';
		$allResults = WSMacro::retreiveKnvbData($path, array('weeknummer' => 'A'));
		if ($allResults["errorcode"] == 1000) {
			$matchDays = array();
			foreach ($allResults["List"] as $result) {
				$matchDay = $result["WedstrijdDag"];
				$matchDayArray = $matchDays[$matchDay];
				if ($matchDayArray == null)
					$matchDayArray = array();
				$matchDayArray[] = $result;
				$matchDays[$matchDay] = $matchDayArray;
			}
			WPSWS_Output::get()->output(array('matchDays' => $matchDays));
		} else
			WPSWS_Output::get()->output(array('call failed' => $allResults, 'path' => $competitieCall));
	}

	public function schedule() {
		global $wpdb;
		$weekStr = $_POST['week'];
		if ($weekStr == null || $weekStr == 0)
			$week = date("W");
		else
			$week = (int) $weekStr;
		$path = "/wedstrijden";
		$params = array('weeknummer' => $week, 'order' => 'time');
		$result = WSMacro::retreiveKnvbData($path, $params);
		//$categories = array();
		$days = array();
		foreach ($result["List"] as $match) {
			$matchDetails = array();
			$matchDay = $match["Datum"];
			$matchId = $match["MatchId"];
			//$addedDetails = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__match_details WHERE match_id=%s", (int) $matchId));
			//if ($addedDetails != null) {
			//	foreach ($addedDetails as $addedDetail) {
			//		$matchDetails[$addedDetail->key] = $addedDetail->value;
			//	}
			//}
			$matchDetails['time'] = $match["Tijd"];
			$matchDetails['compType'] = $match["CompType"];
			$matchDetails['home'] = $match["ThuisClub"];
			$matchDetails['away'] = $match["UitClub"];
			$matchDetails['matchNr'] = $match["WedstrijdNummer"];
			$matchDetails['info'] = $match["Bijzonderheden"];
			$matchDetails['goalsHome'] = $match["PuntenTeam1"];
			$matchDetails['goalsAway'] = $match["PuntenTeam2"];
			$matchDetails['matchId'] = $matchId;
			$matchDetails['quickHome'] = $match['ThuisClubNummer'] == 'BBKT65L'; 
			$compType = $match["CompType"];
			if ($compType == 'R') {
				$matchDetails['compTypeShort'] = 'Co';
				$matchDetails['compType'] = 'Competitie';
			}
			else if ($compType == 'B') {
				$matchDetails['compTypeShort'] = 'Be';
				$matchDetails['compType'] = 'Beker';
			}
			else if ($compType == 'N') {
				$matchDetails['compTypeShort'] = 'Na';
				$matchDetails['compType'] = 'Nacompetitie';
			}
			else {
				$matchDetails['compTypeShort'] = 'Vr';
				$matchDetails['compType'] = 'Vriendschappelijk';
			}
			$homeTeamCode = $match["ThuisClubNummer"];
			$teamId = -1;
			if ($homeTeamCode != 'BBKT65L')
				$teamId = $match["UitTeamId"];
			else
				$teamId = $match["ThuisTeamId"];
			$matchDetails['teamId'] = $teamId;
			$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id = %s", (int) $teamId), ARRAY_A);
			$teamcategory = $team["category"];
			$category = 'unknown';
			$categoryPostfix = 'Home';
			if ($homeTeamCode != 'BBKT65L')
				$categoryPostfix = 'Away';
			if($teamcategory == 'Senioren')
				$category = 'senior';
			else
				$category = 'youth';
			$matchDate = new DateTime($matchDay);
			$dayOfWeek = date_format($matchDate, 'l');
			if ($dayOfWeek == 'Saturday' || $dayOfWeek == 'Sunday')
				$category = $category . 'Weekend';
			else
				$category = $category . 'Week';
			$category = $category . $categoryPostfix;
			$matchDetails['category'] = $teamcategory;
			$matchDetails['displCategory'] = $category;
			$matches = $days[$matchDay];
			if ($matches == null)
				$matches = array();
			$matches[] = $matchDetails;
			$days[$matchDay] = $matches;
			//$categories[$category] = $days;
		}
		WPSWS_Output::get()->output(array('week' => $week, 'matchDays' => $days));
	}
}