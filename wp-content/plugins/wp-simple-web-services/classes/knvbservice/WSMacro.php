<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WSMacro {
	public function getClubInfo($clubCode) {
		$clubInfo = wp_cache_get("ClubInfo");
		if ($clubInfo == null)
			$clubInfo = WSMacro::loadClubInfoFromDB();
		return $clubInfo[$clubCode];
	}
	
	public function loadClubInfoFromDB() {
		global $wpdb;
		$clubs = $wpdb->get_results("SELECT * FROM webservice__clubinfo", ARRAY_A);
		$clubInfo = array();
		foreach($clubs as $club) {
			$info = array();
			$info["website"] = $club["url"];
			$clubInfo[$club["clubcode"]] = $info;
		}
		wp_cache_set("ClubInfo", $clubInfo);
		return $clubInfo;
	}
	
	public function retreiveKnvbData($path, $paramList) {
		global $wpdb;
		$baseUrl = 'http://api.knvbdataservice.nl/api';
		$knvb_session_id = WSMacro::getSessionId();
		$md5key = 'YlUmNBwTZ2HVO6U' . '#' . $path . '#' . $knvb_session_id;
		$hash = md5($md5key);
		$paramString = '';
		if ($paramList != null) {
			foreach ($paramList as $key => $value)
				$paramString = $paramString . '&' . $key . '=' . $value;
		}
		$url = $baseUrl . $path . '?PHPSESSID=' . $knvb_session_id . '&hash=' . $hash . $paramString;
		$response = wp_remote_get($url);
		$responseBody = wp_remote_retrieve_body($response);
		$arr = json_decode($responseBody, true);
		// Check voor foutcodes bij knvb, zoals sessie-id expired
		// TODO: 3x proberen en dan een nette foutmelding terugsturen
		$code = $arr["errorcode"];
		if ($code == 9992 || $code == 9998 || $code == 9996) {
			global $wpdb;
			$wp_session = WP_Session::get_instance();
			$wp_session['knvb_session_id'] = null;
			$baseUrl = 'http://api.knvbdataservice.nl/api';
			$knvb_session_id = WSMacro::getSessionId();
			$wpdb->insert( 'webservice__test', array('value' => 'Needed new SID: ' . $knvb_session_id) );
			$md5key = 'YlUmNBwTZ2HVO6U' . '#' . $path . '#' . $knvb_session_id;
			$hash = md5($md5key);
			$paramString = '';
			if ($paramList != null) {
				foreach ($paramList as $key => $value)
					$paramString = $paramString . '&' . $key . '=' . $value;
			}
			$url = $baseUrl . $path . '?PHPSESSID=' . $knvb_session_id . '&hash=' . $hash . $paramString;
			$response = wp_remote_get($url);
			$responseBody = wp_remote_retrieve_body($response);
			$arr = json_decode($responseBody, true);
		}
		return $arr;
	}

	public function getSessionId() {
		global $wpdb;
		$wp_session = WP_Session::get_instance();	
		$knvb_session_id = $wp_session['knvb_session_id'];
		if ($knvb_session_id == null) {
			$result = $result . '_null_';
			$response = wp_remote_get('http://api.knvbdataservice.nl/api/initialisatie/quick20');
			$responseBody= wp_remote_retrieve_body($response);
			$jsonArray = json_decode($responseBody, true);
			$sessionData= $jsonArray["List"][0];
			$out = $out . ' sdata:' . $sessionData;
			$knvb_session_id = $sessionData["PHPSESSID"];
			$wp_session['knvb_session_id'] = $knvb_session_id;
		}
		return $knvb_session_id;
	}
	
	public function getClubUrl($teamName) {
		global $wpdb;
		$urls = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__clubsite"), ARRAY_A);
		$maxPerc = 0;
		$bestMatch = "";
		foreach ($urls as $url) {
			$name1 = $url["name1"];
			$name2 = $url["name2"];
			$thisPerc = similar_text($name1, $teamName) + similar_text($name2, $teamName);
			if ($thisPerc > $maxPerc) {
				$maxPerc = $thisPerc;
				$bestMatch = $url["url"];
			}
		}
		return $bestMatch;
	}
}