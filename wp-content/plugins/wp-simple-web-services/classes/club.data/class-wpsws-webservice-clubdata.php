<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_clubdata {
	private static $instance = null;
	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_cdTest', array( $this, 'cdTest' ) );
		add_action( 'wpsws_webservice_cdCompetition', array( $this, 'cdCompetition' ) );
		add_action( 'wpsws_webservice_cdScheduleAll', array( $this, 'cdScheduleAll' ) );
		add_action( 'wpsws_webservice_cdResultsAll', array( $this, 'cdResultsAll' ) );
		add_action( 'wpsws_webservice_cdSchedule', array( $this, 'cdSchedule' ) );
		add_action( 'wpsws_webservice_cdTeamSummary', array( $this, 'cdTeamSummary' ) );
		add_action( 'wpsws_webservice_cdMatchDetails', array( $this, 'cdMatchDetails' ) );
		add_action( 'wpsws_webservice_cdResults', array( $this, 'cdResults' ) );
		add_action( 'wpsws_webservice_cdTrainingOverview', array( $this, 'cdTrainingOverview' ) );
		add_action( 'wpsws_webservice_cdPeriodRanking', array( $this, 'cdPeriodRanking' ) );
	}
	
	public function cdPeriodRanking() {
		$pouleCode = $_POST['pouleCode'];
		$periodNr = $_POST['periodNr'];
		$params = array('poulecode' => $pouleCode, 'periodenummer' => $periodNr);
		$periodRankingRaw = CDMacro::getarticle(CDMacro::PERIOD_RANKING, $params);
		// Names are different between period ranking and regular ranking:
		$periodRanking = array();
		foreach ($periodRankingRaw as $teamRaw) {
			$team = array();
			$team["positie"] = $teamRaw["positie"];
			$team["teamnaam"] = $teamRaw["teamnaam"];
			$team["clubrelatiecode"] = $teamRaw["clubcode"];
			$team["gespeeldewedstrijden"] = $teamRaw["aantalwedstrijden"];
			$team["gewonnen"] = $teamRaw["gewonnen"];
			$team["gelijk"] = $teamRaw["gelijkspel"];
			$team["verloren"] = $teamRaw["verloren"];
			$team["doelpuntenvoor"] = $teamRaw["doelpuntenvoor"];
			$team["doelpuntentegen"] = $teamRaw["tegendoelpunten"];
			$team["doelsaldo"] = $teamRaw["doelsaldo"];
			$team["verliespunten"] = $teamRaw["verliespunten"];
			$team["punten"] = $teamRaw["totaalpunten"];
			$periodRanking[] = $team;
		}
		WPSWS_Output::get()->output($periodRanking);
	}
	
	public function cdScheduleAll() {
		$knvbId = $_POST['teamId'];
		$pouleCode = $_POST['pouleCode'];
		$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE', 'aantaldagen' => 300);
		$schedule = CDMacro::getarticle(CDMacro::POULE_SCHEDULE, $params);
		$compSchedule = array();
		foreach ($schedule as $match) {
			$matchDate = $match["wedstrijddatum"];
			$datetime = DateTime::createFromFormat("Y-m-d\TH:i:s+", $matchDate);
			$weekKey = $datetime->format("Y") . $datetime->format("W");
			$weekSchedule = $compSchedule[$weekKey];
			if ($weekSchedule == null)
				$weekSchedule = array();
			$weekSchedule[] = $match;
			$compSchedule[$weekKey] = $weekSchedule;
		}
		WPSWS_Output::get()->output($compSchedule);
	}
	
	public function cdResultsAll() {
		$knvbId = $_POST['teamId'];
		$pouleCode = $_POST['pouleCode'];
		// Start: week 30
		$currWeek = date("W");
		$weekOffset = $currWeek > 30 ? 30 - $currWeek : -22 - $currWeek;
		
		$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE', 
			'aantaldagen' => 300, 'weekoffset' => $weekOffset);
		$schedule = CDMacro::getarticle(CDMacro::POULE_RESULTS, $params);
		$compSchedule = array();
		foreach ($schedule as $match) {
			$matchDate = $match["wedstrijddatum"];
			$datetime = DateTime::createFromFormat("Y-m-d\TH:i:s+", $matchDate);
			$weekKey = $datetime->format("Y") . $datetime->format("W");
			$weekSchedule = $compSchedule[$weekKey];
			if ($weekSchedule == null)
				$weekSchedule = array();
			$weekSchedule[] = $match;
			$compSchedule[$weekKey] = $weekSchedule;
		}
		WPSWS_Output::get()->output($compSchedule);
	}
	
	public function cdTrainingOverview() {
		$trainings = CDMacro::getAllTrainingsData(true);
		WPSWS_Output::get()->output($trainings['trainingDataClub']);
	}
	
	public function cdTeamSummary() {
		$teamId = $_POST['teamId'];
		
		$nextMatch = null;
		$ranking = null;
		$compIn = CDMacro::getCompetitionData($teamId);
		foreach($compIn as $comp) {
			if($comp['competitiesoort'] == 'regulier') {
				$pouleCode = $comp["poulecode"];
				$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'JA', 'aantaldagen' => 90);
				$schedule = CDMacro::getarticle(CDMacro::POULE_SCHEDULE, $params);
				if($schedule != null)
					$nextMatch = $schedule[0];
				
				$params = array('poulecode' => $pouleCode);
				$ranking = CDMacro::getarticle(CDMacro::POULE_RANKING, $params);
			}
		}
		WPSWS_Output::get()->output(array('nextMatch' => $nextMatch, 'ranking' => $ranking));
	}
	
	public function cdMatchDetails() {
		$matchId = $_POST['matchId'];
		// Match details
		// https://data.sportlink.com/wedstrijd-informatie?client_id=CRqGQ9TjfU&wedstrijdcode=12618778
		$params = array('wedstrijdcode' => $matchId);
		$details = CDMacro::getArticle(CDMacro::MATCH_DETAILS, $params);
		
		// Competition ranking
		$pouleCode = $details['wedstrijdinformatie']['poulecode'];
		if ($pouleCode != null && $pouleCode > 0) {
			$params = array('poulecode' => $pouleCode);
			$ranking = CDMacro::getarticle(CDMacro::POULE_RANKING, $params);
			$details['ranking'] = $ranking;
		}
			
		WPSWS_Output::get()->output(array('details' => $details, 'params' => $params));
	}
	
	
	public function cdTest() {
		$result = DateTime::createFromFormat("Y-m-d", "2017-01-01"); 
		WPSWS_Output::get()->output($result->format("W"));
	}
	
	public function cdSchedule() {
		global $wpdb;
		
		/* Explanation:
			We can only request a weekoffset (x weeks from today) and nr of days ahead.
			So we have to request more days than we need and throw away the matches
			that are not in the requested week. Otherwise we would miss e.g. Monday - Friday
			if we request week x on Saturday.
			
			$currDayOfWeek = the nr of today (1=Monday, etc)
			$actualOffset = the week to request
			$daysAhead = nr of days to look ahead
			$scheduleUnfiltered = matches before filtering the extra days
			$schedule = matches after filtering
		*/
		
		// Determine the week to request:
		$weekStr = $_POST['week'];
		$currWeek = date("W");
		$weekStr = $weekStr == 0 ? $currWeek : $weekStr;
		
		if ($weekStr >= 30 && $currWeek < 30)
			$currWeek += 52;
		if ($weekStr < 30 && $currWeek >= 30)
		    $weekStr += 52;
		$weekOffset = $weekStr - $currWeek;
		$currDayOfWeek = date("N");
		$actualOffset = $currDayOfWeek == 0 ? $weekOffset : $weekOffset - 1;
		$daysAhead = $currDayOfWeek == 0 ? 7 : 15 - $currDayOfWeek;
		
		//https://data.sportlink.com/programma?client_id=CRqGQ9TjfU&weekoffset=0&aantaldagen=7
		$params = array('weekoffset' => $actualOffset, 'aantaldagen' => $daysAhead);
		$scheduleUnfiltered = CDMacro::getarticle(CDMacro::ALL_SCHEDULE, $params);
		$schedule = array();
		foreach ($scheduleUnfiltered as $unf) {
			$date = new DateTime($unf["wedstrijddatum"]);
			$dateWeek = $date->format("W");
			if ($dateWeek == $weekStr)
				$schedule[] = $unf;
		}

		// Also get the results (this has to be a separate call):
		//https://data.sportlink.com/uitslagen?client_id=CRqGQ9TjfU&weekoffset=0&aantaldagen=7
		//$params = array('weekoffset' => $actualOffset - 1, 'aantaldagen' => $daysAhead);
		$results = CDMacro::getarticle(CDMacro::ALL_RESULTS, $params);
		$resultsFixedDate = array();
		foreach ($results as $res) {
			// Date fields are different for results and schedule :(
			$matchDate = date_create($res['datum']);
			$matchWeek = date_format($matchDate, "W");
			$matchYear = date_format($matchDate, "Y");
			if ($matchWeek == $weekStr && $matchYear >= 2018) {
				$res['datum'] = $res['datumopgemaakt'];
				$resultsFixedDate[] = $res;
			}
		}
		// Sort on date/time
		$scheduleAndResults = $this->quick_sort(array_merge($schedule, $resultsFixedDate));
				
		$days = array();
		$alreadyProcessedMatches = array();
		foreach ($scheduleAndResults as $match) {
			$matchDetails = array();
			$matchDay = $match["datum"];
			$matchId = $match["wedstrijdcode"];
			if(in_array($matchId, $alreadyProcessedMatches)) continue;
			$alreadyProcessedMatches[] = $matchId;
			$matchDetails['time'] = $match["aanvangstijd"];
			$matchDetails['compType'] = $compType;
			$matchDetails['home'] = $match["thuisteam"];
			$matchDetails['away'] = $match["uitteam"];
			$matchDetails['matchNr'] = $match["wedstrijdnummer"];
			$matchDetails['status'] = $match["status"];
			$matchDetails['matchId'] = $matchId;
			$matchDetails['quickHome'] = $match['thuisteamclubrelatiecode'] == 'BBKT65L'; 
			$matchDetails['klkHome'] = $match['kleedkamerthuisteam'];
			$matchDetails['klkAway'] = $match['kleedkameruitteam'];
			$matchDetails['ref'] = $match['scheidsrechters'];
			$matchDetails['field'] = $match['veld'];
			$matchDetails['result'] = $match['uitslag'];
			$matchDetails['week'] = $match['week'];
			// $comp = $match["competitie"];
			$compS = $match["competitiesoort"];
			// $compType = 'Vr';
			if (strpos($compS, 'beker') !== false || strpos($compS, 'Cup') !== false) {
				$matchDetails['compTypeShort'] = 'Be';
				$matchDetails['compType'] = 'Beker';
			}
			else if ($compS == 'nacompetitie') {
				$matchDetails['compTypeShort'] = 'Na';
				$matchDetails['compType'] = 'Nacompetitie';
			}
			else if ($compS != null && $compS != '') {
				$matchDetails['compTypeShort'] = 'Co';
				$matchDetails['compType'] = 'Competitie';
			}
			else {
				$matchDetails['compTypeShort'] = 'Vr';
				$matchDetails['compType'] = $compS;
			}
			$homeTeamCode = $match["thuisteamclubrelatiecode"];
			$teamId = -1;
			if ($homeTeamCode != 'BBKT65L')
				$teamId = $match["uitteamid"];
			else
				$teamId = $match["thuisteamid"];
			$matchDetails['teamId'] = $teamId;
			$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id = %s", (int) $teamId), ARRAY_A);
			$teamcategory = $team["category"];
			$category = 'unknown';
			$categoryPostfix = 'Home';
			if ($homeTeamCode != 'BBKT65L')
				$categoryPostfix = 'Away';
			if($teamcategory == 'Senioren')
				$category = 'senior';
			else
				$category = 'youth';
			$matchDay2 = $match['wedstrijddatum'];
			$matchDate = new DateTime($matchDay2);
			$dayOfWeek = date_format($matchDate, 'l');
			if ($dayOfWeek == 'Saturday' || $dayOfWeek == 'Sunday')
				$category = $category . 'Weekend';
			else
				$category = $category . 'Week';
			$category = $category . $categoryPostfix;
			$matchDetails['category'] = $teamcategory;
			$matchDetails['displCategory'] = $category;
			$matches = $days[$matchDay];
			if ($matches == null)
				$matches = array();
			$matches[] = $matchDetails;
			$days[$matchDay] = $matches;
		}
		WPSWS_Output::get()->output(array('week' => $weekStr, 'matchDays' => $days, 'params' => $params));
	}
	
	public function cdCompetition() {
		$teamcode = $_POST['teamcode'];
		$params = array('teamcode' => $teamcode, 'lokaleteamcode' => -1);
		$compIn = CDMacro::getCompetitionData($teamcode);
		$compOut = array();
		$regCompPouleCode = null;
		if ($compIn != null) {
			// First: check for VOORJAAR / NAJAAR:
			$voorjaarPouleCode = null;
			$najaarPouleCode = null;
			foreach ($compIn as $competition) {
				$compType = $competition["competitiesoort"];
				if ($compType == 'regulier') {
					$pouleCode = $competition["poulecode"];
					$regCompPouleCode = $pouleCode;
					if (strpos($competition["competitienaam"], 'voorjaar') !== false)
						$voorjaarPouleCode = $pouleCode;
					else if (strpos($competition["competitienaam"], 'najaar') !== false)
						$najaarPouleCode = $pouleCode;
				}
			}
			$doubleSeason = $voorjaarPouleCode != null && $najaarPouleCode != null;
			$currWeek = date("W");
			$activePouleCode = $currWeek < 30 && $currWeek > 4 ? $voorjaarPouleCode : $najaarPouleCode;
			if ($doubleSeason)
				$regCompPouleCode = $activePouleCode;
			foreach ($compIn as $competition) {
				$comp = array();
				$pouleCode = $competition["poulecode"];
				if(!$pouleCode)
				    continue;
				$compType = $competition["competitiesoort"];
				if ($compType == 'regulier' && $doubleSeason && $pouleCode != $activePouleCode)
					continue;
				$comp['details'] = $competition;
				// Schedule:
				//https://data.sportlink.com/poule-programma?client_id=CRqGQ9TjfU&poulecode=xxxxx&eigenwedstrijden=NEE
				$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE', 'aantaldagen' => 21);
				$schedule = CDMacro::getarticle(CDMacro::POULE_SCHEDULE, $params);
				if (sizeof($schedule) < 6) {
					$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE', 'aantaldagen' => 42);
					$schedule = CDMacro::getarticle(CDMacro::POULE_SCHEDULE, $params);
				}
				$compSchedule = array();
				if (!$schedule['error']) {
    				foreach ($schedule as $match) {
    					if ($compType == 'regulier') {
    						$matchDate = $match["wedstrijddatum"];
    						$datetime = DateTime::createFromFormat("Y-m-d\TH:i:s+", $matchDate);
    						$weekNr = $datetime->format("W");
    						$weekSchedule = $compSchedule[$weekNr];
    						if ($weekSchedule == null)
    							$weekSchedule = array();
    						$weekSchedule[] = $match;
    						$compSchedule[$weekNr] = $weekSchedule;
    					} else {
    						$compSchedule[] = $match;
    					}
    				}
				}
				$comp['schedule'] = $compSchedule;
				
				// Ranking:
				$params = array('poulecode' => $pouleCode);
				$ranking = CDMacro::getarticle(CDMacro::POULE_RANKING, $params);
				if ($compType == 'regulier' || sizeof($ranking) < 10)
					$comp['ranking'] = $ranking;
				
				// Results:
				if ($compType == 'regulier')
					$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE');
				else {
					$weekOffset = $currWeek > 30 ? 30 - $currWeek : -22 - $currWeek;
					$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE',
						'weekoffset' => $weekOffset, 'aantaldagen' => 250);
				}
				$compResults = CDMacro::getarticle(CDMacro::POULE_RESULTS, $params);
				$comp['results'] = $compResults;
				
				// Periods:
				if ($compType == 'regulier') {
					$params = array('poulecode' => $pouleCode);
					$periods = CDMacro::getarticle(CDMacro::PERIOD_NUMBERS, $params);
					if ($periods != null && $periods[0]["waarde"] != null) {
						$comp['hasPeriods'] = true;
						$comp['periods'] = $periods;
					}
				}
				
				$compOut[] = $comp;
			}
		}
		
		// Add training data:
		global $wpdb;
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $teamcode), ARRAY_A);
		$shortName = $team['short_name'];
		if ($team["category"] == 'Senioren' && strpos($shortName, 'VR') === false && strpos($shortName, 'beker') === false)
			$shortName = $team["group"] . $shortName; 
		$allTrainingsData = CDMacro::getAllTrainingsData(true);
		$trainingData = $allTrainingsData['trainingDataTeam'][$shortName]; 
		WPSWS_Output::get()->output(array('competitions' => $compOut, 'regCompPouleCode' => $regCompPouleCode,
		    'trainingData' => $trainingData));
	}
	
	// --- Hulpfuncties --- //
	
	function quick_sort($array) {
		$length = count($array);
		if($length <= 1)
			return $array;
		else {
			$pivot = $array[0];
			$left = $right = array();
			for($i = 1; $i < count($array); $i++) {
				if($array[$i]['wedstrijddatum'] < $pivot['wedstrijddatum'])
					$left[] = $array[$i];
				else
					$right[] = $array[$i];
			}
			return array_merge($this->quick_sort($left), array($pivot), $this->quick_sort($right));
		}
	}
}