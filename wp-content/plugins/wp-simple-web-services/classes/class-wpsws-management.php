<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Mangement {

	private static $instance = null;

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$role = get_role('editor');
		$role->add_cap('manage_sportlink');
		$role->add_cap('manage_fabri');
		$role->add_cap('manage_teampic');
		$role->add_cap('manage_clubinfo');
		$role = get_role('administrator');
		$role->add_cap('manage_sportlink');
		$role->add_cap('manage_fabri');
		$role->add_cap('manage_teampic');
		$role->add_cap('manage_clubinfo');
		$role->add_cap('manage_jubileum');
		$role = get_role('manage_fabri');
		$role->add_cap('manage_fabri');
		$role = get_role('manage_jubileum');
		$role->add_cap('manage_jubileum');
		add_action( 'admin_menu', array( $this, 'add_menu_pages' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	public function add_menu_pages() {
		add_menu_page( 'Overview', 'Beheer Quick', 'niet-gebruikt', 'beheer', null );
		add_submenu_page(
			'beheer', 'Teams en spelers', 'Teams en spelers', 'manage_sportlink', 'beheer-sportlink', array($this, 'upload_sportlink')
		);
		add_submenu_page(
			'beheer', 'Fabri League', 'Fabri League', 'manage_fabri', 'beheer-fabri', array($this, 'upload_fabri')
		);
		add_submenu_page(
			'beheer', 'Teamfoto\'s', 'Teamfoto\'s', 'manage_teampic', 'beheer-teampic', array($this, 'upload_teampic')
		);
		add_submenu_page(
			'beheer', 'Clubinformatie', 'Clubinformatie', 'manage_clubinfo', 'beheer-clubinfo', array($this, 'manage_clubinfo')
		);
		add_submenu_page(
			'beheer', 'Jubileum', 'Jubileum', 'manage_jubileum', 'beheer-jubileum', array($this, 'manage_jubileum')
		);
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'wpw-admin', plugin_dir_url( WPSWS_PLUGIN_FILE ) . '/assets/js/jquery.q20.admin.js', array( 'jquery' ), '1.0.0' );
		wp_enqueue_style( 'admin_css', plugin_dir_url( WPSWS_PLUGIN_FILE ) . '/assets/css/q20.admin.css', false, '1.0.0' );
	}
	
	public function manage_jubileum() {
		global $wpdb;
		?>
		<div class="wrap" id="wpw-wrap">
			<div>
				<h1>Ideeënbus jubileum</h1>
			</div>
			<br/><br/>
			<table class="JubileumTable">
				<tr>
					<th>Datum</th>
					<th>Naam</th>
					<th>Email</th>
					<th>Bericht</th>
					<th></th>
				</tr>
				<?php
				$ideas = $wpdb->get_results($wpdb->prepare("SELECT * FROM jubileum"), ARRAY_A);
				foreach($ideas as $idea) {
				?>
				<tr>
					<td><?php echo $idea["date"] ?></td>
					<td><?php echo $idea["name"] ?></td>
					<td><?php echo $idea["email"] ?></td>
					<td><?php echo nl2br($idea["comment"]) ?></td>
					<td class="delete" db_id="<?php echo $idea["id"] ?>">verwijderen</td>
				</tr>
				<?php } ?>
			</table>
		</div>
	<?php
	}
	
	public function manage_clubinfo() {
		?>
		<div class="wrap" id="wpw-wrap">
			<div>
				<h1>Beheer clubs</h1>
			</div>
			<br/><br/>
			<div class="LoadingInProgress">
				Clubs worden ingeladen, een moment geduld a.u.b.
			</div>
			<div class="ClubInfo">
				<div class="Message"></div>
				<form action="/webservice/updateClubinfo" method="post">
					In dit overzicht staan alle clubs waar een team van Quick dit seizoen of een vorig seizoen</br>
					(sinds seizoen 2016/2017) tegen gespeeld heeft.</br>
					<br/>
					Ontbreekt er een club voor dit seizoen? Klik dan op <a class="LoadLink" href="/webservice/loadClubinfo">deze link</a>.<br/>
					Dit duurt ongeveer een halve minuut.<br/>
					<br/>
					<div class="uitleg">
						<div class="titel">
							<a>Waarom zie ik in de tabel teamnamen staan, en geen clubnamen?</a>
						</div>
						<div class="tekst">
							Goede vraag!<br/>
							<br/>
							De KNVB levert ons alleen de namen van teams aan, samen met de clubcode.<br/>
							Ze levert echter geen CLUBnamen aan. Vandaar dat je namen als Achilles '12 MO13-2</br>
							in de lijst ziet staan in plaats van gewoon Achilles '12.<br/>
							<br/>
							De websites worden WEL zichtbaar voor alle teams van de club. Dit gaat namelijk <br/>
							op basis van clubcode, en niet op basis van teamnaam.<br/>
							<br/>
							Bedankt voor het up-to-date houden van quick20.nl!
						</div>
					</div>
					Maak hieronder je wijzigingen en klik daarna op: <input type="submit" value="Opslaan"></input>
					<div class="DataRow header">
						<div class="one">Code</div>
						<div class="two">Club</div>
						<div class="three">Website</div>
					</div>
					<?php
						global $wpdb;
						$clubs = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__clubinfo ORDER BY name ASC" ), ARRAY_A);
						foreach ($clubs as $club) { 
							$existing = $club["url"] != null && $club["url"] != "";
					?>
						<div class="DataRow">
							<input type="hidden" name="id[]" value="<?php echo $club["id"] ?>"></input>
							<div class="one"><?php echo $club["clubcode"] ?></div>
							<div class="two"><?php echo $club["name"] ?></div>
							<div class="three<?php if ($existing) echo ' existing' ?>"><input type="text" name="url[]" value="<?php echo $club["url"] ?>"></div>
						</div>
					<?php } ?>
				</form>
			</div>
		</div>
	<?php
	}
	
	public function upload_teampic() {
		?>
		<div class="wrap" id="wpw-wrap">
			<div>
				<h1>Upload teamfoto's</h1>
				<br/><br/>
				<h3>Selecteer een team:</h3><br/>
				Kies een teams:
				<select name="team_id" class="team_select">
				<?php
				global $wpdb;
				$teams = $wpdb->get_results("SELECT * FROM webservice__team ORDER BY `group`, CHAR_LENGTH(short_name), short_name", ARRAY_A);
				foreach ($teams as $team) { 
					$displName = $team["short_name"];
					if ($team["category"] == "Senioren")
						$displName = $displName . " (" . $team["group"] . ")" ?>
						<option value="<?php echo $team['knvb_id']; ?>"><?php echo $displName ?></option>
					<?php
				}
				?>
				</select>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Deze teams hebben nog geen teamfoto:
				<select>
				<?php
				global $wpdb;
				$teams = $wpdb->get_results("SELECT * FROM webservice__team t LEFT JOIN webservice__team_data d ON t.knvb_id = d.team_id WHERE d.id IS NULL ORDER BY `group`, CHAR_LENGTH(short_name), short_name", ARRAY_A);
				foreach ($teams as $team) { 
					$displName = $team["short_name"];
					if ($team["category"] == "Senioren")
						$displName = $displName . " (" . $team["group"] . ")" ?>
						<option><?php echo $displName ?></option>
					<?php
				}
				?>
				</select>
				<br/><br/>
				<h3>Upload nieuwe teamfoto:</h3>
				<form id="upload_teamfoto" method="post" enctype="multipart/form-data">
					<input type="hidden" name="team_id" class="form_team_id"></input>
					<input type="file" name="file" id="file"></input>
					<input type="submit" value="Upload teamfoto" name="submit" id="submitbutton"></input>
				</form>
				<div id="loading">
					<img src="/wp-admin/images/quick-beheer_loading.gif"/>
				</div>
				<br/><br/>
				<h3>Huidige foto <span id="current_name"></span>:</h3>
				<div class="delete_current" style="display: none;">
					<form id="delete_teamfoto" method="post">
						<input type="hidden" name="team_id" class="form_team_id"></input>
						<input type="submit" value="Foto verwijderen" name="submit" id="submitbutton"></input>
					</form>
				</div>
				<br/>
				<div id="current_pic">
				</div>
			</div>
		</div>
	<?php
	}
	
	public function upload_sportlink() {
		?>
		<div class="wrap" id="wpw-wrap">
			<div>
				<h1>Upload teams en spelers vanuit Sportlink</h1>
				<h3>Uitleg:</h3>
				<ul>
				 <li>Open sportlink</li>
				 <li>Klik op 'Zoeken (=)'</li>
				 <li>Selecteer het tabblad 'Teams'</li>
				 <li>Selecteer alle bondsteams en alle lokale teams (Ctrl-A)</li>
				 <li>Klik rechtsonder op het vergrootglas</li>
				 <li>Klik rechtsboven op de knop 'Kolommen wijzigen'</li>
				 <li>Zorg ervoor dat de volgende kolommen in elk geval in de lijst met zichtbare kolommen staan:</li>
				 <li><b>Team, Teamcode, Relatienr, Roepnaam, Tussenvoegsel, Achternaam, E-mail, Mobiel, Leeftijdscat team, Teamfunctie, Team rol, Op wedstrijdform?</b></li>
				 <li>Klik op 'Ok'</li>
				 <li>Klik rechtsonder op deze knop: &nbsp;&nbsp;&nbsp; <img style="width: 50px;" src="/wp-admin/images/quick-beheer_button-sportlink.png"/></li>
				 <li>Selecteer alle beschikbare kolommen (Ctrl-A) en klik op de blauwe pijl naar rechts: &nbsp;&nbsp;&nbsp;<img style="width: 40px;" src="/wp-admin/images/quick-beheer_sportlink-columns.png"/></li>
				 <li>Klik op 'Verder'</li>
				 <li>Klik op 'Voltooien'</li>
				 <li>Sla het bestand op met .csv als extensie. Bijvoorbeeld: export-sportlink-01-01-2016.csv</li>
				 <li>Klik op verzenden.</li>
				 <li>Het uploaden kan even duren. Na afloop komt er feedback terug.</li>
				</ul>
				<div id="csv_upload">
					<form action="/webservice/csvupload/" method="post" enctype="multipart/form-data">
						<label for="file">Selecteer een csv:</label> 
						<input id="file" type="file" name="file" /> <br/>
						<input id="file" type="checkbox" name="nopl_y">Verwijder spelers jeugd</input><br/>
						<input id="file" type="checkbox" name="nopl_s">Verwijder spelers senioren</input><br/>
						<input id="submit" type="submit" name="submit" value="Verzenden" />
					</form>
				</div>
			</div>
		</div>
	<?php
	}
	
	public function upload_fabri() {
		?>
		<div class="wrap" id="wpw-wrap">
			<div>
				<h1>Upload uitslagen en stand Fabri-league</h1>
				<div>
					<form action="/webservice/fabriupload/" method="post" enctype="multipart/form-data">
						<label for="file">Selecteer een excel-bestand:</label> 
						<input id="file" type="file" name="file" /> 
						<input id="submit" type="submit" name="submit" value="Verzenden" />
					</form>
				</div>
			</div>
		</div>
	<?php
	}
}